<?php include "include/header.php"; ?>
<?php include "include/menu.php";
if(!isset($_SESSION["email"])) {
	echo "<script>alert('You do not have permission to acces this page.')</script>";
  header("location: login");
  die();
} 


?>

<div id="bread_crumb">Success </div><br>
<?php echo $_SESSION['customer_id']; ?><br>
Payment successful.

Below is the payment details:
<?php   foreach($_SESSION["cart"] as $row) { 
	$price=0;
	$discountedAmount=0;
	$discountedPrice=0;
	$vatAmount=0;
	$del_charge = 0;
	$shippingstatus= "Not Shipped Yet!!";
            $paymentstatus= "Paid";
 ?>

<div>
	<ul>
		<li>Product ID</li>
		<li><?php echo $row['pid']; ?><br><br></li>
	</ul>
	<ul>
		<li>Product Name</li>
		<li><?php echo $row['pname']; ?><br><br></li>
	</ul>
	<ul>
		<li>Size</li>
		<li><?php echo $row['size']; ?><br><br></li>
	</ul>
	<ul>
		<li>qty</li>
		<li><?php echo $row['qty'];?><br><br></li>
	</ul>
	<ul>
		<li>Color</li>
		<li><?php echo $row['color']; ?><br><br></li>
	</ul>
	
	<ul>
		<li>Rate</li>
		<li><?php echo $row['price']; ?><br><br></li>
	</ul>
	<ul>
		<li>Price = Rate * Quantity</li>
		<li><?php $price= $row['price'] * $row['qty']; 
				  echo $price; ?><br><br></li>
	</ul>
	<ul>
		<li>Price After Discount(<?php echo $row['discount']; ?>% )</li>
		<li><?php  $discountedAmount= ($row['discount']/100)*$price;
					$discountedPrice= $price-$discountedAmount;
					echo $discountedPrice;?>
				  </li><br><br>
	</ul>
	<ul>
		<li>VAT : 13% </li>
		<li><?php $vatAmount= $discountedPrice*0.13;
				  echo$vatAmount; ?></li><br><br>
	</ul>
	<ul>
		<li>Delivery Charge</li>
		<li><?php if($del_charge == "0"){ echo "Free";}else{echo "+ ".$del_charge; } ?><br><br></li>
	</ul>
	<ul>
		<li>Price after Vat and Discount and Delivery Charge</li>
		<li><?php  echo$vatAmount+$discountedPrice+$del_charge ?></li><br><br>
	</ul>
	<ul>
		<li>Purchase Date</li>
		<li><?php  $date = strftime('%c');
			       echo $date; ?><br><br></li>
	</ul>
	
	<ul>
		<li>Shipment Details</li>
		<li>Customer Address</li><br><br><hr><br><br>
	</ul>
</div>
<?php 
$obj = new Database();
          
       
            $data = array(
                'product_id'        => $row['pid'],
                'customer_id'    =>  $_SESSION['customer_id'],
                'qty'  => $row['qty'],
                'rate'   => $row['price'],
                'discount'       => $discountedAmount,
                'total_price'   => $vatAmount+$discountedPrice+$del_charge,
                'shipping_status'      => $shippingstatus,
                'payment_status'   => $paymentstatus,
                'sale_date'      => $date    
            );  
            
            $obj -> insert("sales", $data); 
            
     
?>
<?php } ?>

<?php  unset($_SESSION['cart']); // will delete just the name data
 ?>



