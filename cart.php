<?php include "include/header.php"; ?>
<?php include "include/menu.php"; ?>
<div id="bread_crumb">Cart </div>
<div id="cart_wrap">

	<?php error_reporting(0); ?>

	<?php 
		if($_SESSION['cart'] == "") {
			echo "Not item avaible in Cart";
			die;
		} 
	?>
	<?php

		//create blank to array to push cart value for paypal api use
		$apname 	= array();
		$aqty		= array();
		$aamount	= array();
		$c = 0;
		$grandtotal = 0;
	?>

	<div id=cartdetails_wrap>

		<?php foreach($_SESSION['cart'] as $row) { $c++ ?>	

		<?php 
			$qty = $row['qty'];
			$price = $row['price'];
			$bdamount = $qty * $price;
			$discountrate = $row['discount']/100;
			$admount =  $bdamount * $discountrate;
			$total = $bdamount - $admount;
			$vat = 13;
			$del_charge = 0;
		?>

		<div id="cartdetails">
			<h2><?php echo $row['pname'] ?></h2>
			<ul>
				<li><img src="product_img/<?php echo $row['image'] ?>"></li>
			</ul>
			<ul>
				<li>Quantity : <?php echo $row['qty'] ?></li>
				<li>Size : <?php echo $row['size'] ?></li>
				<li></li>
			</ul>
			<ul>
				<li>Price Per Unit</li>
				<li><?php echo $row['price'] ?></li>
			</ul>
			<ul>
				<li>Discount</li>
				<li><?php echo $row['discount'] ?>%</li>
			</ul>			
			<ul class="amt">
				<li>Total Amount</li>
				<li>Rs. <?php echo $total ?></li>		
			</ul>
			<?php 
				$carttotal += $total;
				$vatamt 	= $carttotal * ($vat/100);
			?>
		</div>

		<?php 

			//insert cart value to above array for paypal api use
			$nameString 	= '<input type="hidden" name="item_name_' . $c . '" value="' . $row['pname'] . '" />';
	        $amountString 	= '<input type="hidden" name="amount_' . $c . '" value="' . $total . '" />';
	        $quantityString = '<input type="hidden" name="quantity_' . $c . '" value="' . $row['qty'] . '" />';

			array_push($apname, $nameString);
			array_push($aqty, $quantityString);
			array_push($aamount, $amountString);

			$_SESSION['apname'] 	= $apname;
			$_SESSION['aqty']		= $aqty;
			$_SESSION['aamount'] 	= $aamount;
		?>

		<?php } ?>

	</div>

	<div id="checkoutdetails">

		<h2>Price Details :</h2>

		<ul>
			<li>Cart Total</li>
			<li><?php echo $carttotal ?></li>
		</ul>
		<ul>
			<li>Vat (<?php echo $vat; ?>%)</li>
			<li> +<?php echo $vatamt; ?></li>
		</ul>
		<ul>
			<li>Delivery Charge</li>
			<li><?php if($del_charge == "0"){ echo "Free";}else{echo "- ".$del_charge; }?></li>
		</ul>
		<ul class="amt">
			<li>Grand Total :</li>
			<li><?php echo $carttotal + $vatamt + $del_charge ?></li>
		</ul>

		<div class="clear"></div>

	  	<ul class="cartbtn">
		 	<li><a href="checkout">Process Checkout</a></li>
		</ul>
	</div>

</div>

<?php include "include/footer.php"; ?>