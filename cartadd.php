<?php session_start();

	$image 		= $_POST['image'];
	$pname 		= $_POST['pname'];
	$pid 		= $_POST['pid'];
	$pcode 		= $_POST['pcode'];
	$qty 		= $_POST['qty'];
	$size 		= $_POST['size'];
	$price 		= $_POST['price'];
	$discount 	= $_POST['discount'];
	$color 		= $_POST['color'];

	$data = array(
		"image"		=> $image,
		"pname"		=> $pname,
		"pid"		=> $pid,
		"pcode"		=> $pcode,
		"qty"		=> $qty,
		"size"		=> $size,
		"price"		=> $price,
		"discount"	=> $discount,
		"color"		=> $color
	);

	$_SESSION["cart"][] = $data;

	header("location:cart")
?>