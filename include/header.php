<?php session_start();
	require 'cPanel/content/class/database.php';
	$obj = new Database();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ePeepal</title>

	<meta name="keywords" content="online shopping, online shopping sites, online shopping nepal, nepal shopping" />
  	<meta name="description" content="Online Shopping site in Nepal for Branded Shoes, Clothing, Accessories for Men and Women & many more." />
  
    <link rel="stylesheet" href="library/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="library/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="library/css/default.css">
    <link rel="stylesheet" href="library/css/jquery-ui.css">
   	<link rel="stylesheet" type="text/css" href="library/engine1/style.css" />

    <script src="library/js/jquery-3.0.0.min.js"></script>
	<script src="library/engine1/jquery.js"></script>
	<script src="library/js/color-box.js"></script>
	<script src="library/js/jquery-ui.js"></script>

</head>

<script>            
	jQuery(document).ready(function() {
		var offset = 220;
		var duration = 500;
		jQuery(window).scroll(function() {
			if (jQuery(this).scrollTop() > offset) {
				jQuery('.scroll-top').fadeIn(duration);
			} else {
				jQuery('.scroll-top').fadeOut(duration);
			}
		});
 
		jQuery('.scroll-top').click(function(event) {
			event.preventDefault();
			jQuery('html, body').animate({scrollTop: 0}, duration);
			return false;
		})
	});
</script>

<script type="text/javascript">
  
  $(function() {
    $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: 5000,
      values: [ 0, 5000 ],
      slide: function( event, ui ) {
        $( "#amount" ).html(ui.values[ 0 ] + " - " + ui.values[ 1 ] );
		$( "#amount1" ).val(ui.values[ 0 ]);
		$( "#amount2" ).val(ui.values[ 1 ]);
      }
    });
    $( "#amount" ).html($( "#slider-range" ).slider( "values", 0 ) +
     " - " + $( "#slider-range" ).slider( "values", 1 ) );
  });
  </script>
  <script>
    function change(val){
        abc = document.getElementById("fimg");
        abc.src = val;
    }
  </script>
<script type="text/javascript">
	function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
</script>
<script>
$(document).ready(function()
                  {
$('input[type="checkbox"]').on('change', function(e) {
		var data = {};
		var dataStrings = [];
		
		$('input[type="checkbox"]').each(function() {
			if (this.checked) {
				if (data[this.name] === undefined) data[this.name] = [];
				data[this.name].push(this.value);
			}
		});
		
		$.each(data, function(key, value)
		{
		    dataStrings.push(key + "=" + value.join(','));
		});
		
		var result = dataStrings.join('&');

		//alert(result);
		
		$.post('ajax-post-url.php', data);
		if (history.pushState) {
			history.pushState(null, null, loc.pathname + '?' + data);
		}
	});
 });
</script>
<body id="body">

	<!-- Header Start -->
	
	<div id="header_wrap">
		<a href="http://localhost/epeepal_old">
			<div id="logo">
				<ul>
					<li><img src="library/images/ep_logo.png"></li>
					<li id="name">ePeepal</li>
					<?php if(isset($_SESSION["email"])){
     	echo"<br><br><li>Welcome   &nbsp".$_SESSION["email"]."   &nbsp<a href='logout'>Logout</a></li>";

     } ?>
				</ul>
			</div>
		</a>	
		<div id="top_contain">
			<div id="search_box">
			<ul>
				<li><input type="text" name=""></li>
				<li><input type="submit" value="" name=""></li>
			</ul>
			</div>
			<div id="userpanel">
				<ul>
					<li id="loginaccount">
						<div id="my_account">
                            <div id="login_form">
                            	<form method="POST" action="customer_login_process">
                                <ul>
                                    <li><input type="text" name="email" placeholder="Type Email"></li>
                                    <li><input type="password" name="password" placeholder="Type Password"></li>
                                    <li class="forget"><a href="">Forget Password?</a></li>
                                    <li class="yellow_btn"><input type="submit" value="Login" name="submit"></li>
                                </ul>
                                </form>
                                <ul>
                                    <li>New here?</li>
                                    <li class="register_btn"><a href="">Register</a></li>
                                </ul>
                            </div>
                        </div>
					</li>
					<a href="cart">
						<span>
							<li id="cart"></li>
							<li id="carttittle">Cart</li>
							<li id="cartnum">
								<span>
								<?php
									if(!isset($_SESSION['cart'])){
										echo "0";
									}else{
										$c = count($_SESSION['cart']);
										echo $c;
									}
								?>							
								</span>
							</li>
						</span>
					</a>
				</ul>
			</div>
		</div>
	</div>
	<div class="clear"></div>
	<!-- Header End -->