<?php
	//active = 1, inactive = 0
	$status = 1;
	$cat 	= $obj->selectRequiredRow_Sub("category","status",$status,"order_id");
	$group 	= $obj->selectRequiredRow_Sub("group_table","status",$status,"name");
	$subcat 	= $obj->selectRequiredRow_Sub("sub_category","status",$status,"name");
	//print_r($sub_menu);
?>	
	<!-- Menu Start -->

	<div id="menu_wrap">
		<ul>
			<?php foreach($cat as $menu_row)  {?>
			<li><a href="<?php echo $menu_row['page'] ?>?c=0"><?php echo $menu_row['name']; ?></a>  
				<div class="submenu_wrap">
					<div class="submenu">
						<?php foreach ($group as $grow)  { 
							if($menu_row['category_id'] == $grow['cat_id']){ 
						?>
						<ul>
							<li><h2><?php echo $grow['name'] ?></h2></li>
							<?php foreach ($subcat as $subrow) {
								if($grow['id']==$subrow['group_id']){
							?>
							<li><a href="<?php echo $menu_row['page']; ?>?c=0&item=<?php echo $subrow['name'] ?>"><?php echo $subrow['name'] ?></a></li>
							<?php } }?>
						</ul>
						<?php } } ?>
					</div>
				</div>
			</li>
			<?php } ?>
		</ul>
	</div>

	<!-- Menu End -->
