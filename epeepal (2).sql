-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 31, 2017 at 05:20 PM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `epeepal`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE IF NOT EXISTS `cart` (
  `id` int(11) NOT NULL,
  `cart_session` varchar(250) NOT NULL,
  `image` varchar(250) NOT NULL,
  `product_name` varchar(250) NOT NULL,
  `product_id` varchar(250) NOT NULL,
  `product_code` varchar(250) NOT NULL,
  `quantity` int(5) NOT NULL,
  `product_price` float NOT NULL DEFAULT '0',
  `total_price` float NOT NULL,
  `cart_status` varchar(250) NOT NULL,
  `order_id` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `cart_session`, `image`, `product_name`, `product_id`, `product_code`, `quantity`, `product_price`, `total_price`, `cart_status`, `order_id`) VALUES
(2, '693332248', '540771484-iphone4s.png', 'iPhone 4s', '1', '196081681', 1, 45000, 45000, '', ''),
(3, '530653211', '888977050-dealtime-dt3.png', 'DealTime Speaker', '2', '143668184', 1, 1500, 1500, '', ''),
(4, '879638671', '903839111-hisense32in.png', 'Hisense TV 32 Inc', '3', '542895947', 1, 28000, 28000, '', ''),
(5, '481635199', '540771484-iphone4s.png', 'iPhone 4s', '1', '196081681', 3, 45000, 135000, '', ''),
(6, '840494791', '888977050-dealtime-dt3.png', 'DealTime Speaker', '2', '143668184', 1, 1500, 1500, '', ''),
(7, '525526258', '540771484-iphone4s.png', 'iPhone 4s', '1', '196081681', 1, 45000, 45000, '', ''),
(8, 'mr4eog5fh38m2r0fe7bv3k5h15', '540771484-iphone4s.png', 'iPhone 4s', '1', '196081681', 1, 45000, 45000, '', ''),
(9, 'mr4eog5fh38m2r0fe7bv3k5h15', '540771484-iphone4s.png', 'iPhone 4s', '1', '196081681', 1, 45000, 45000, '', ''),
(10, 'fv5tpft672l8h691n4fa6ulg06', '540771484-iphone4s.png', 'iPhone 4s', '1', '196081681', 1, 45000, 45000, '', ''),
(11, 'fv5tpft672l8h691n4fa6ulg06', '540771484-iphone4s.png', 'iPhone 4s', '1', '196081681', 1, 45000, 45000, '', ''),
(12, 'fv5tpft672l8h691n4fa6ulg06', '540771484-iphone4s.png', 'iPhone 4s', '1', '196081681', 1, 45000, 45000, '', ''),
(13, '3p41ra2rladm4f8rgq5h2hgt42', '895935058-micromax-m523.png', 'MicorMax M435', '4', '140290705', 1, 12000, 12000, '', ''),
(14, '3p41ra2rladm4f8rgq5h2hgt42', '477233886-iphone6plus.png', 'iPhone 6 Plus', '5', '839674654', 1, 95000, 95000, '', ''),
(15, '3p41ra2rladm4f8rgq5h2hgt42', '3326416-lg-l365.png', 'LG L 365', '6', '1258275064', 1, 45, 45, '', ''),
(16, '3p41ra2rladm4f8rgq5h2hgt42', '903839111-hisense32in.png', 'Hisense TV 32 Inc', '3', '542895947', 1, 28000, 28000, '', ''),
(17, '3p41ra2rladm4f8rgq5h2hgt42', '540771484-iphone4s.png', 'iPhone 4s', '1', '196081681', 1, 45000, 45000, '', ''),
(18, '3p41ra2rladm4f8rgq5h2hgt42', '540771484-iphone4s.png', 'iPhone 4s', '1', '196081681', 1, 45000, 45000, '', ''),
(19, '3p41ra2rladm4f8rgq5h2hgt42', '895935058-micromax-m523.png', 'MicorMax M435', '4', '140290705', 1, 12000, 12000, '', ''),
(20, '5l9ft71vfsj2d754p98l571c03', '3326416-lg-l365.png', 'LG L 365', '6', '1258275064', 1, 45000, 45000, '', ''),
(21, '5l9ft71vfsj2d754p98l571c03', '888977050-dealtime-dt3.png', 'DealTime Speaker', '2', '143668184', 1, 1500, 1500, '', ''),
(22, '5l9ft71vfsj2d754p98l571c03', '903839111-hisense32in.png', 'Hisense TV 32 Inc', '3', '542895947', 1, 28000, 28000, '', ''),
(23, '3pgal2dq4v4qvbp914a2a6aog2', '540771484-iphone4s.png', 'iPhone 4s', '1', '196081681', 1, 45000, 45000, '', ''),
(24, '3pgal2dq4v4qvbp914a2a6aog2', '540771484-iphone4s.png', 'iPhone 4s', '1', '196081681', 1, 45000, 45000, '', ''),
(25, '3pgal2dq4v4qvbp914a2a6aog2', '540771484-iphone4s.png', 'iPhone 4s', '1', '196081681', 1, 45000, 45000, '', ''),
(26, 'qftkifmkbhnc4ehjjs1oqua106', '477233886-iphone6plus.png', 'iPhone 6 Plus', '5', '839674654', 1, 95000, 95000, '', ''),
(27, 'dcem1dicnb6kuhr3oen4sgrpe5', '888977050-dealtime-dt3.png', 'DealTime Speaker', '2', '143668184', 1, 1500, 1500, '', ''),
(28, 'j1bg5pajn07d404f3u8gl42bf3', '477233886-iphone6plus.png', 'iPhone 6 Plus', '5', '839674654', 1, 95000, 95000, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `category_id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL,
  `status` varchar(11) NOT NULL,
  `order_id` int(2) NOT NULL,
  `page` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `name`, `description`, `status`, `order_id`, `page`) VALUES
(14, 'MEN', 'All Men Items', '1', 1, 'men'),
(16, 'BABIES, KIDS & TOY', 'All Babies Items', '1', 3, 'kids'),
(17, 'LIQUOR & TOBACCO', 'All Groceries Items', '1', 6, 'liquor'),
(18, 'ELECTRONICS & APPLIANCES', 'All electronic Items', '1', 4, 'electronics'),
(26, 'MORE', 'MORE', '1', 7, 'more'),
(27, 'WOMEN', 'WOMEN', '1', 2, 'women'),
(29, 'GROCERIES', 'all Groceries', '1', 5, 'groceries');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `customer_id` int(11) NOT NULL,
  `first_name` varchar(250) NOT NULL,
  `last_name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `mobile` varchar(250) NOT NULL,
  `country` varchar(250) NOT NULL,
  `zone` varchar(250) NOT NULL,
  `district` varchar(250) NOT NULL,
  `full_address` varchar(250) NOT NULL,
  `enter_date` varchar(250) NOT NULL,
  `exclusive_offer` varchar(3) NOT NULL,
  `login_status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customer_id`, `first_name`, `last_name`, `email`, `password`, `mobile`, `country`, `zone`, `district`, `full_address`, `enter_date`, `exclusive_offer`, `login_status`) VALUES
(1, 'sagar', 'kafle', 'nephopsagar@live.com', 'kafle', '', '', '', '', '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `group_table`
--

CREATE TABLE IF NOT EXISTS `group_table` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `cat_id` varchar(250) NOT NULL,
  `status` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group_table`
--

INSERT INTO `group_table` (`id`, `name`, `cat_id`, `status`) VALUES
(4, 'Clothing', '14', '1'),
(5, 'Footwear', '14', '1'),
(6, 'Men''s Grooming', '14', '1'),
(7, 'Watches', '14', '1'),
(8, 'Bags, Belts and Wallets', '14', '1'),
(9, 'Accessories', '14', '1'),
(10, 'test', '21', '1'),
(11, 'test', '24', '1'),
(12, 'tst', '25', '1'),
(19, 'Beauty & Hygiene', '27', '1'),
(20, 'Clothing', '27', '1'),
(22, 'Baby Care', '16', '1'),
(23, 'Clothing & Accessories', '16', '1'),
(24, 'Toys & Games', '16', '1'),
(25, 'Mobiles & Tablets', '18', '1'),
(26, 'Televisions', '18', '1'),
(27, 'Laptops & Desktops', '18', '1'),
(28, 'Cameras & Accessories', '18', '1'),
(29, 'Audio & Video ', '18', '1'),
(30, 'Softwares', '18', '1'),
(31, 'Computer Accessories', '18', '1'),
(32, 'Kitchen Appliances', '18', '1'),
(33, 'Home Appliances', '18', '1'),
(34, 'Make up & Cosmetics', '27', '1'),
(35, 'Jewellery ', '27', '1'),
(36, 'Watches ', '27', '1'),
(37, ' Bags, Belts & Clutches ', '27', '1'),
(38, 'Footwear ', '27', '1'),
(40, 'Whisky ', '17', '1'),
(41, 'Bourbon ', '17', '1'),
(42, 'Vodka ', '17', '1'),
(43, 'Brandy ', '17', '1'),
(44, 'Gin ', '17', '1'),
(45, 'Beer ', '17', '1'),
(46, 'Rum ', '17', '1'),
(47, 'Books', '26', '1'),
(48, ' Health & Wellness ', '26', '1'),
(49, ' Home & Decor ', '26', '1'),
(50, 'Sports ', '26', '1'),
(51, ' Pets & Pet Accessories ', '26', '1'),
(52, ' Biking & Accessories ', '26', '1'),
(53, ' Mountain Bikes ', '26', '1');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL,
  `order_id` varchar(250) NOT NULL,
  `order_date` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `product_id` int(11) NOT NULL,
  `product_code` varchar(250) NOT NULL,
  `category` varchar(250) NOT NULL,
  `category_id` varchar(250) NOT NULL,
  `group_name` varchar(250) NOT NULL,
  `group_id` varchar(250) NOT NULL,
  `sub_category` varchar(250) NOT NULL,
  `sub_category_id` varchar(250) NOT NULL,
  `name` varchar(250) NOT NULL,
  `photo1` varchar(250) NOT NULL,
  `photo2` varchar(250) NOT NULL,
  `photo3` varchar(250) NOT NULL,
  `photo4` varchar(250) NOT NULL,
  `price` float DEFAULT '0',
  `discount` int(250) NOT NULL,
  `color` varchar(250) NOT NULL,
  `size1` varchar(2) NOT NULL,
  `size2` varchar(2) NOT NULL,
  `size3` varchar(2) NOT NULL,
  `size4` varchar(2) NOT NULL,
  `stock` int(11) NOT NULL DEFAULT '1',
  `product_caption` varchar(250) NOT NULL,
  `material_care` varchar(250) NOT NULL,
  `product_desc` varchar(250) NOT NULL,
  `other_details` varchar(250) NOT NULL,
  `entry_date` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `product_code`, `category`, `category_id`, `group_name`, `group_id`, `sub_category`, `sub_category_id`, `name`, `photo1`, `photo2`, `photo3`, `photo4`, `price`, `discount`, `color`, `size1`, `size2`, `size3`, `size4`, `stock`, `product_caption`, `material_care`, `product_desc`, `other_details`, `entry_date`) VALUES
(0, '1051750797', 'BABIES, KIDS & TOY', '16', 'Clothing & Accessories', '23', 'Nepali Dress', '78', 'NIce Dress', '805419921-+2 transcript 001.jpg', '792510986-IMG_20170103_101406.jpg', '619812011-sagar nagarikata 001 (1).jpg', '342498779-IMG_20170103_101355.jpg', 1200, 3, 'Red', 'EL', 'L', 'M', 'S', 120, 'Nice Product', 'Nice Product', 'Nice Product', 'Nice Product', '31 03 2017 09:03:04'),
(37, '211768269', 'MEN', '14', 'Clothing', '4', 'Formal Shirt', '23', 'Formal Shirt', '174621582-a1.jpg', '254241943-a2.jpg', '792236328-a3.jpg', '105499267-a4.jpg', 850, 0, '1', 'EL', 'L', 'M', 'S', 10, 'This is Formal Shirt for men', '90% Cotton\r\n10% Silk', 'Best Quality\r\n6 Months Warranty', 'Free Shipping\r\nBuy Back Gurantee', '03 09 2016 10:58:12'),
(38, '653388237', 'MEN', '14', 'Clothing', '4', 'T-Shirt', '22', 'Men Tshirt ', '783813476-b1.jpg', '999908447-b2.jpg', '538635253-b3.jpg', '556732177-b4.jpg', 750, 5, '#SDFDFDd', 'EL', 'L', 'M', 'S', 25, 'Caption...........', 'material.........', 'desc.........', 'other..........', '03 09 2016 11:06:52'),
(39, '743162992', 'WOMEN', '27', 'Clothing', '20', 'T-Shirts', '65', 'Women Tshirt', '452392578-i1.jpg', '62530517-i2.jpg', '801940917-i3.jpg', '944549560-i4.jpg', 750, 0, '#SDFDFDd', 'EL', 'L', 'M', 'S', 10, 'caption', 'material', 'desc', 'other', '03 09 2016 11:11:28'),
(40, '589524831', 'MEN', '14', 'Clothing', '4', 'Winter & Casuals Wear', '24', 'Casual Pants ', '722961425-q1.jpg', '273284912-q2.jpg', '117919921-q3.jpg', '855010986-q4.jpg', 1250, 10, '#SDFDFDd', '', 'L', 'M', '', 25, 'caption........', 'care.......', 'desc......', 'other..........', '03 09 2016 11:15:45'),
(41, '766623568', 'MEN', '14', 'Clothing', '4', 'Sports Wear', '25', 'Sport Wear', '816680908-e1.jpg', '879333496-e2.jpg', '755340576-e3.jpg', '349487304-e4.jpg', 1200, 5, 'red', '', 'L', 'M', 'S', 10, 'caption.....', 'care.......', 'desc.....', 'other.......', '03 09 2016 11:17:53'),
(42, '1295811990', 'MEN', '14', 'Clothing', '4', 'T-Shirt', '22', 'Men Tshirt', '33355712-j1.jpg', '59387207-j2.jpg', '693695068-j3.jpg', '692626953-j4.jpg', 1200, 1, '#SDFDFDd', 'EL', 'L', 'M', '', 25, 'caption......', 'care......', 'desc........', 'other.......', '03 09 2016 11:19:00'),
(43, '896571484', 'MEN', '14', 'Clothing', '4', 'T-Shirt', '22', 'Casul stripe Men', '888671875-f1.jpg', '241973876-f2.jpg', '486267089-f3.jpg', '161102294-f4.jpg', 1200, 10, '1', 'EL', 'L', 'M', '', 25, 'caption,,,,,', 'materail......', 'des.........', 'other...', '03 09 2016 11:20:09'),
(44, '219360107', 'WOMEN', '27', 'Clothing', '20', ' Winter & Casuals ', '154', 'Women Casual Tshirt', '485534667-i1.jpg', '456268310-i2.jpg', '875610351-i3.jpg', '5767822-i4.jpg', 650, 0, '#SDFDFDd', '', 'L', 'M', 'S', 10, 'cap......', 'car....', 'des....', 'oth...', '03 09 2016 11:21:52'),
(45, '173200524', 'MEN', '14', 'Clothing', '4', 'Formal Shirt', '23', 'Formal Shirt', '432312011-h1.jpg', '904998779-h2.jpg', '37231445-e3.jpg', '872467041-e4.jpg', 950, 0, '#SDFDFDd', '', 'L', 'M', '', 10, 'cap.........', 'mat.......', 'des........', 'oth.......', '03 09 2016 11:53:40'),
(46, '847918073', 'MEN', '14', 'Clothing', '4', 'Winter & Casuals Wear', '24', 'Casual Nike Tshirt', '897521972-a1.jpg', '925384521-a2.jpg', '321777343-a3.jpg', '700469970-a4.jpg', 850, 0, '#SDFDFDd', 'EL', 'L', 'M', '', 25, 'capt.....', 'mat.....', 'des.......', 'oth.....', '03 09 2016 11:55:59');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE IF NOT EXISTS `sales` (
`id` int(11) NOT NULL,
  `product_id` varchar(250) NOT NULL,
  `customer_id` varchar(250) NOT NULL,
  `qty` varchar(250) NOT NULL,
  `rate` int(255) NOT NULL,
  `discount` int(255) NOT NULL,
  `total_price` varchar(250) NOT NULL,
  `shipping_status` varchar(250) NOT NULL,
  `payment_status` varchar(250) NOT NULL,
  `sale_date` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id`, `product_id`, `customer_id`, `qty`, `rate`, `discount`, `total_price`, `shipping_status`, `payment_status`, `sale_date`) VALUES
(1, '38', '1', '2', 750, 75, '1610.25', 'Not Shipped Yet!!', 'Paid', '09/14/16 06:49:21'),
(3, '37', '1', '1', 850, 0, '960.5', 'Not Shipped Yet!!', 'Paid', '09/15/16 04:20:18'),
(4, '38', '1', '1', 750, 38, '805.125', 'Not Shipped Yet!!', 'Paid', '09/15/16 04:20:18'),
(5, '38', '1', '1', 750, 38, '805.125', 'Not Shipped Yet!!', 'Paid', '09/15/16 04:45:24'),
(6, '41', '1', '1', 1200, 60, '1288.2', 'Not Shipped Yet!!', 'Paid', '09/15/16 04:45:24');

-- --------------------------------------------------------

--
-- Table structure for table `sub_category`
--

CREATE TABLE IF NOT EXISTS `sub_category` (
  `id` int(11) NOT NULL,
  `cat_id` varchar(250) NOT NULL,
  `group_id` varchar(250) NOT NULL,
  `name` varchar(250) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_category`
--

INSERT INTO `sub_category` (`id`, `cat_id`, `group_id`, `name`, `status`) VALUES
(22, '14', '4', 'T-Shirt', '1'),
(23, '14', '4', 'Formal Shirt', '1'),
(24, '14', '4', 'Winter & Casuals Wear', '1'),
(25, '14', '4', 'Sports Wear', '1'),
(26, '4', '14', 'test', '1'),
(33, '14', '5', 'Casual Shoes ', '1'),
(34, '14', '5', 'Converse', '1'),
(35, '14', '5', 'Sports Shoes', '1'),
(36, '14', '5', 'Shoe Care', '1'),
(37, '14', '5', 'Vans', '1'),
(38, '14', '5', 'Socks', '1'),
(39, '14', '6', 'Body Spray & Deodorants', '1'),
(40, '14', '6', 'Bath Soap / Body Wash', '1'),
(41, '14', '6', 'Shampoo', '1'),
(42, '14', '6', 'Hair Oil & Hair Cream', '1'),
(43, '14', '6', 'Face Wash', '1'),
(44, '14', '6', 'Cream & Lotion', '1'),
(45, '14', '6', 'Shaving Cream & Shaving Kit', '1'),
(46, '14', '6', 'Dental Hygiene Products', '1'),
(47, '14', '6', 'Tooth Paste & Brush ', '1'),
(48, '14', '6', 'Hair Color', '1'),
(49, '14', '7', 'Titan', '1'),
(50, '14', '7', 'Fastrack', '1'),
(51, '14', '7', 'Timex', '1'),
(52, '14', '7', 'Nautica', '1'),
(53, '14', '7', 'Rado', '1'),
(54, '14', '8', 'Backpacks', '1'),
(55, '14', '8', 'Belts & Wallets ', '1'),
(56, '14', '9', 'Adidas', '1'),
(57, '14', '9', 'Caps', '1'),
(58, '14', '9', 'Sunglasses', '1'),
(59, '14', '9', 'Silver Coins', '1'),
(60, '14', '9', 'Ornaments', '1'),
(63, '27', '19', 'Shampoo & Conditioner ', '1'),
(65, '27', '20', 'T-Shirts', '1'),
(66, '27', '20', 'Sarees', '1'),
(68, '16', '22', 'Baby Food', '1'),
(69, '16', '22', 'Baby Oil', '1'),
(70, '16', '22', 'Baby Lotion', '1'),
(71, '16', '22', 'Baby Powder', '1'),
(73, '16', '22', 'Baby Shampoo & Soap', '1'),
(75, '16', '22', 'Diaper & Wet Wipes', '1'),
(76, '16', '23', 'Pasni Dress', '1'),
(77, '16', '23', 'Bedding Set', '1'),
(78, '16', '23', 'Nepali Dress', '1'),
(79, '16', '24', 'Baby Toys', '1'),
(80, '16', '24', 'Swimming Pool & Accessories', '1'),
(81, '16', '24', 'Various', '1'),
(82, '18', '29', 'Home Theatre', '1'),
(83, '18', '29', 'Projectors ', '1'),
(84, '18', '29', 'Speakers ', '1'),
(85, '18', '29', 'iPods ', '1'),
(86, '18', '29', ' MP3 & MP4 Players ', '1'),
(87, '18', '28', 'Sony', '1'),
(88, '18', '28', 'Samsung', '1'),
(89, '18', '28', 'Cannon', '1'),
(90, '18', '28', 'BenQ', '1'),
(91, '18', '31', 'Cables', '1'),
(92, '18', '31', 'Internal HDD Storage', '1'),
(93, '18', '31', ' Mouse & Keyboard ', '1'),
(94, '18', '31', 'Portable Storage', '1'),
(95, '18', '31', ' Laptop & Desktop RAM ', '1'),
(96, '18', '31', ' Flash Drives ', '1'),
(97, '18', '31', 'Routers & Wireless', '1'),
(98, '18', '31', ' Printers & Accessories ', '1'),
(99, '18', '31', 'Chargers ', '1'),
(100, '18', '31', 'Others ', '1'),
(101, '18', '33', 'Refrigerator & Freezers ', '1'),
(102, '18', '33', ' Vaccum Cleaners ', '1'),
(103, '18', '33', ' Washing Machines ', '1'),
(104, '18', '33', ' Air Conditioners ', '1'),
(105, '18', '33', ' Fans ', '1'),
(106, '18', '33', ' Clothes Dryer', '1'),
(107, '18', '33', ' Inverter & Power Supply ', '1'),
(108, '18', '33', 'Heaters & Irons ', '1'),
(109, '18', '33', 'Geysers ', '1'),
(110, '18', '32', 'Cookware ', '1'),
(111, '18', '32', ' Rice Cookers ', '1'),
(112, '18', '32', 'Microwave Oven &  Mixer Grinders ', '1'),
(113, '18', '32', 'Electric Oven & Gas Stoves & Hobs ', '1'),
(114, '18', '32', 'Dish Washers', '1'),
(115, '18', '32', ' Electric Kettles ', '1'),
(116, '18', '32', 'Water Purifiers', '1'),
(117, '18', '32', 'Others ', '1'),
(118, '18', '27', 'Mac ', '1'),
(119, '18', '27', 'Lenovo ', '1'),
(120, '18', '27', 'ASUS ', '1'),
(121, '18', '27', 'Fujitsu ', '1'),
(122, '18', '27', 'Dell', '1'),
(123, '18', '27', 'Acer', '1'),
(124, '18', '27', 'Others', '1'),
(125, '18', '25', 'iPhone ', '1'),
(126, '18', '25', 'Samsung ', '1'),
(127, '18', '25', 'Sony ', '1'),
(128, '18', '25', 'LG ', '1'),
(129, '18', '25', 'Colors', '1'),
(130, '18', '25', 'Huawei ', '1'),
(131, '18', '30', 'Antivirus', '1'),
(132, '18', '30', 'MicroSoft Office Package', '1'),
(133, '18', '30', 'Windows Utilities', '1'),
(134, '18', '26', 'Samsung ', '1'),
(135, '18', '26', 'LG ', '1'),
(136, '18', '26', 'Sony ', '1'),
(137, '18', '26', 'CG ', '1'),
(138, '18', '26', 'Yasuda ', '1'),
(139, '18', '26', ' TV Accessories', '1'),
(140, '27', '19', ' Bath Soap/Body Wash ', '1'),
(141, '27', '19', ' Deo Spray/Deodorants ', '1'),
(142, '27', '19', ' Hair Care Products ', '1'),
(143, '27', '19', ' Nail Polish ', '1'),
(144, '27', '19', ' Lip Balm & Chapstick ', '1'),
(145, '27', '19', ' Skin Care ', '1'),
(146, '27', '19', ' Sanitary Pads & Tampons ', '1'),
(147, '27', '19', 'Cream & Sun Block ', '1'),
(148, '27', '37', 'Handbags ', '1'),
(149, '27', '37', ' Korean Bags ', '1'),
(150, '27', '37', ' Clutches & Purses ', '1'),
(151, '27', '37', 'Belts ', '1'),
(152, '27', '20', ' Kurti & Kurta Salwars ', '1'),
(153, '27', '20', ' Shirts, Tops & Tunics ', '1'),
(154, '27', '20', ' Winter & Casuals ', '1'),
(155, '27', '20', 'Sportswear ', '1'),
(156, '27', '38', 'Flats ', '1'),
(157, '27', '38', 'Heels ', '1'),
(158, '27', '38', 'Boots ', '1'),
(159, '27', '38', ' Sports Shoes ', '1'),
(160, '27', '38', 'Casual ', '1'),
(161, '27', '38', 'Converse ', '1'),
(162, '27', '35', 'Necklaces ', '1'),
(163, '27', '35', 'Earrings ', '1'),
(164, '27', '35', 'Bangles & Kadas ', '1'),
(165, '27', '35', 'Rings ', '1'),
(166, '27', '35', 'Gold ', '1'),
(167, '27', '35', 'Diamond ', '1'),
(168, '27', '34', 'Garnier ', '1'),
(169, '27', '34', 'L''Oreal ', '1'),
(170, '27', '34', 'Maybelline ', '1'),
(171, '27', '36', 'Titan ', '1'),
(172, '27', '36', 'Timex ', '1'),
(173, '27', '36', 'Fastrack ', '1');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `user` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `first_name` varchar(250) NOT NULL,
  `last_name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `user_type` varchar(5) NOT NULL,
  `status` varchar(250) NOT NULL,
  `login_status` int(2) NOT NULL,
  `role` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `user`, `password`, `first_name`, `last_name`, `email`, `user_type`, `status`, `login_status`, `role`) VALUES
(14, 'superAdmin', 'superAdmin', 'Admin of', 'Cpanel', 'admin@esant.com', 'User', '1', 0, 'superAdmin'),
(15, 'admin', 'admin', 'administrator', 'administrator', 'administrator', '', '', 0, 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
 ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
 ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `group_table`
--
ALTER TABLE `group_table`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `order_id` (`order_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
 ADD UNIQUE KEY `product_id` (`product_id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
