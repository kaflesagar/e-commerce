<?php require "include/header.php"; ?>
<?php require "include/menu.php"; ?>
<?php 
	
	if(!isset($_GET['id'])){
		include "404.php";
		exit;
	}

	$id = $_GET['id'];
	$p = $obj->selectRequiredRow("product","product_id",$id);
?>
<script type="text/javascript">
$(window).load(function(){
$('input[type="checkbox"]').on('change', function() {
    $('input[name="' + this.name + '"]').not(this).prop('checked', false);
});
});
</script>

	<div id="bread_crumb">Men / Clothing / T-shirt </div>

	<div id="leftpanel">
		<div id="imgthumb">
			<ul>
				<li><img src="product_img/<?php echo $p['photo1']; ?>" onmouseover="change('product_img/<?php echo $p['photo1']; ?>')"></li>
				<li><img src="product_img/<?php echo $p['photo2']; ?>" onmouseover="change('product_img/<?php echo $p['photo2']; ?>')"></li>
				<li><img src="product_img/<?php echo $p['photo3']; ?>" onmouseover="change('product_img/<?php echo $p['photo3']; ?>')"></li>
				<li><img src="product_img/<?php echo $p['photo4']; ?>" onmouseover="change('product_img/<?php echo $p['photo4']; ?>')"></li>
			</ul>
		</div>
		<div id="fullimg">
			<img src="product_img/<?php echo $p['photo1']; ?>" id="fimg">
		</div>
	</div>
	<div id="rightpanel">
		<form method="POST" action="cartadd">
			<div id="pdetailsl">
				<input type="hidden" name="image" value="<?php echo $p['photo1'] ?>">
				<input type="hidden" name="pname" value="<?php echo $p['name'] ?>">
				<input type="hidden" name="pid" value="<?php echo $p['product_id'] ?>">
				<input type="hidden" name="pcode" value="<?php echo $p['product_code'] ?>">
				<input type="hidden" name="price" value="<?php echo $p['price'] ?>">
				<input type="hidden" name="discount" value="<?php echo $p['discount'] ?>">
				<input type="hidden" name="color" value="<?php echo $p['color'] ?>">
				<ul>
					<li class="pname"><?php echo $p['name']; ?></li>
					<li class="price">Rs. <?php echo $p['price']; ?>.00</li>
					<li class="color">Color : Black [<?php echo $p['color'] ?>]</li>
					<li>
						<span>Select Size</span>
						<div class="size-box">
							<label><input type="checkbox" name="size" value="Large"><span>L</span></label>
							<label><input type="checkbox" name="size" value="Midum"><span>M</span></label>
							<label><input type="checkbox" name="size" value="Small"><span>S</span></label>
						</div>
					</li>
					<li>
						<label>Select Qty </label>
						<input type="number" value="1" min="1" max="10" name="qty" onkeypress='return isNumberKey(event)'>
					</li>
				</ul>
				<div class="clear"></div>
				<div id="buybtn">
					<ul>
						<li><button type="submit" class="btn">Add to Cart</button></li>
						<li><a href="">Add to wishlist</a></li>
					</ul>
				</div>
				<div class="clear"></div>
				<div id="sharebox">
					<ul>
						<li><a href=""><img src="library/images/shareicon.jpg" /></a></li>
						<li></li>
						<li></li>
						<li></li>
					</ul>
				</div>
			</div>	
			<div id="pdetailsr">
				<div id="share"
					<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57c90988a2e6a101"></script>
				</div>
				<div id="pdetails">
					<div id="shortcaption">
						<p><?php echo $p['product_caption']; ?></p>
					</div>
					<div>
						<h2>Product Details</h2>
						<p><?php echo $p['product_desc']; ?></p>		
					</div>
					<div>
						<h2>Material & Care</h2>
						<p><?php echo $p['material_care']; ?></p>
					</div>
					<div>
						<h2>Other Details :</h2>
						<p><?php echo $p['other_details']; ?></p>
					</div>
				</div>
			</div>
		</form>
	</div>

	<div class="clear"></div>

	<div id="similarbox">
		<h2>Similar Product</h2>
		<div id="productlist">
			<div class="productbox">
				<ul>
					<li class="productimg"><img src="product_img/3.jpg"></li>
					<li class="productname">Nike White Casual </li>
					<li class="productprice">Price : </li>
				</ul>
				<div class="product_act">
					<ul>
						<li><a href="product?id=123">View Details</a></li>
					</ul>
				</div>
			</div>

			<div class="productbox">
				<ul>
					<li class="productimg"><img src="product_img/2.jpg"></li>
					<li class="productname">Name </li>
					<li class="productprice">Price : </li>
				</ul>
				<div class="product_act">
					<ul>
						<li><a href="">View Details</a></li>
					</ul>
				</div>
			</div>

			<div class="productbox">
				<ul>
					<li class="productimg"><img src="product_img/1.jpg"></li>
					<li class="productname">Name </li>
					<li class="productprice">Price : </li>
				</ul>
				<div class="product_act">
					<ul>
						<li><a href="">View Details</a></li>
					</ul>
				</div>
			</div>

			<div class="productbox">
				<ul>
					<li class="productimg"><img src="product_img/4.jpg"></li>
					<li class="productname">Name </li>
					<li class="productprice">Price : </li>
				</ul>
				<div class="product_act">
					<ul>
						<li><a href="">View Details</a></li>
					</ul>
				</div>
			</div>

			<div class="productbox">
				<ul>
					<li class="productimg"><img src="product_img/1.jpg"></li>
					<li class="productname">Nike White Casual </li>
					<li class="productprice">Price : </li>
				</ul>
				<div class="product_act">
					<ul>
						<li><a href="">View Details</a></li>
					</ul>
				</div>
			</div>		

		</div>
	</div>

<?php require "include/footer.php"; ?>