<?php 
	$top1 = $obj->selectAlllimit("product","entry_date","0","5");
	$top2 = $obj->selectAlllimit("product","entry_date","5","10");

	$new1 = $obj->selectAlllimit("product","entry_date","5","10");
	$new2 = $obj->selectAlllimit("product","entry_date","0","5");

	$elec 	= $obj->selectReqRowlimit("group_table","cat_id","18","id","0","6");
	$women 	= $obj->selectReqRowlimit("group_table","cat_id","27","id","0","6");
	$men 	= $obj->selectReqRowlimit("group_table","cat_id","14","id","0","6");

?>	
	<!-- Top Promo Panel -->
	<div id="top_promo_panel">
		<ul>
			<li><a href=""><img src="library/images/p1.jpg"></a></li>
			<li><a href=""><img src="library/images/p2.jpg"></a></li>
			<li><a href=""><img src="library/images/p3.jpg"></a></li>
		</ul>
	</div>

	<div class="clear"></div>

	<!-- Product Panel  -->
	<div class="product_panel_wrap">

		<div class="product_tittle">
			<ul>
				<li class="tittle_desc">TOP TRADING</li>
				<li class="tittle_dot"></li>
			</ul>
		</div>
		
		<div class="clear"></div>

		<div class="product_loop">
			<?php foreach($top1 as $trow) { ?>
			<div class="product_wrap">
				<ul class="p_details">
					<li class="p_img"><a href="product?id=<?php echo $trow['product_id'] ?>"><img src="product_img/<?php echo $trow['photo1'] ?>"></a></li>
					<li class="p_name"><?php echo $trow['name'] ?></li>
					<li class="p_amount">Rs. <?php echo $trow['price'] ?></li>
				</ul>
				<ul class="p_option">
					<li><a href="product?id=<?php echo $trow['product_id'] ?>">View More</a></li>
				</ul>
			</div>	
			<?php } ?>
		</div>
		<div class="clear"></div>
		<div class="product_loop">
			<?php foreach($top2 as $trow) { ?>
			<div class="product_wrap">
				<ul class="p_details">
					<li class="p_img"><a href="product?id=<?php echo $trow['product_id'] ?>"><img src="product_img/<?php echo $trow['photo1'] ?>"></a></li>
					<li class="p_name"><?php echo $trow['name'] ?></li>
					<li class="p_amount">Rs. <?php echo $trow['price'] ?></li>
				</ul>
				<ul class="p_option">
					<li><a href="product?id=<?php echo $trow['product_id'] ?>">View More</a></li>
				</ul>
			</div>	
			<?php } ?>
		</div>
	</div>

	<div class="clear">	</div>

	<div class="product_panel_wrap">

		<div class="product_tittle">
			<ul>
				<li class="tittle_desc">NEW ARRIVALS</li>
				<li class="tittle_dot"></li>
			</ul>
		</div>
		
		<div class="clear"></div>

		<div class="product_loop">
			<?php foreach($new1 as $trow) { ?>
			<div class="product_wrap">
				<ul class="p_details">
					<li class="p_img"><a href="product?id=<?php echo $trow['product_id'] ?>"><img src="product_img/<?php echo $trow['photo1'] ?>"></a></li>
					<li class="p_name"><?php echo $trow['name'] ?></li>
					<li class="p_amount">Rs. <?php echo $trow['price'] ?></li>
				</ul>
				<ul class="p_option">
					<li><a href="cart?id=<?php echo $trow['product_id'] ?>">Add to Cart</a></li>
				</ul>
			</div>	
			<?php } ?>
		</div>
		<div class="clear"></div>
		<div class="product_loop">
			<?php foreach($new2 as $trow) { ?>
			<div class="product_wrap">
				<ul class="p_details">
					<li class="p_img"><a href="product?id=<?php echo $trow['product_id'] ?>"><img src="product_img/<?php echo $trow['photo1'] ?>"></a></li>
					<li class="p_name"><?php echo $trow['name'] ?></li>
					<li class="p_amount">Rs. <?php echo $trow['price'] ?></li>
				</ul>
				<ul class="p_option">
					<li><a href="cart?id=<?php echo $trow['product_id'] ?>">Add to Cart</a></li>
				</ul>
			</div>	
			<?php } ?>
		</div>
	</div>

	<div class="clear"></div>

	<div class="quick_category">

		<div class="quick_cat_header">
			<ul>
				<li>ELECTRONIC & APPLIANCES</li>
			</ul>
		</div>

		<div class="clear"></div>

		<div class="quick_cat_desc">
			<div class="quick_cat_menu">
				<ul>
					<li class="quick_menu_tittle">Top Categories</li>
					<?php foreach($elec as $erow) { ?>
					<li><a href=""><?php echo $erow['name'] ?></a></li>
					<?php } ?>
				</ul>
			</div>
			<div class="quick_cat_items">
				<div class="q_cat_main">
					<ul>
						<li><img src="product_img/qimg1.jpg"></li>
					</ul>
					<ul>
						<li><span>New Arrivals</span></li>
						<li><a href="">Shop Now</a></li>
					</ul>
				</div>
				<div class="q_cat_sub">
					<ul>
						<li><img src="product_img/qimg2.jpg"></li>
					</ul>
					<ul>
						<li><span>Intex Power Bank</span></li>
						<li><span>Upto 60% Discount</span></li>
						<li><a href="">Shop Now</a></li>
					</ul>
				</div>
				<div class="q_cat_sub2">
					<ul>
						<li><img src="product_img/qimg3.jpg"></li>
					</ul>
					<ul>
						<li><span>Intex Power Bank</span></li>
						<li><span>Upto 60% Discount</span></li>
						<li><a href="">Shop Now</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="clear"></div>

	<div class="quick_category">

		<div class="quick_cat_header">
			<ul>
				<li>WOMEN</li>
			</ul>
		</div>

		<div class="clear"></div>

		<div class="quick_cat_desc">
			<div class="quick_cat_menu">
				<ul>
					<li class="quick_menu_tittle">Top Categories</li>
					<?php foreach($women as $erow) { ?>
					<li><a href=""><?php echo $erow['name'] ?></a></li>
					<?php } ?>
				</ul>
			</div>
			<div class="quick_cat_items">
				<div class="q_cat_main">
					<ul>
						<li><img src="product_img/qimg1.jpg"></li>
					</ul>
					<ul>
						<li><span>New Arrivals</span></li>
						<li><a href="">Shop Now</a></li>
					</ul>
				</div>
				<div class="q_cat_sub">
					<ul>
						<li><img src="product_img/qimg2.jpg"></li>
					</ul>
					<ul>
						<li><span>Intex Power Bank</span></li>
						<li><span>Upto 60% Discount</span></li>
						<li><a href="">Shop Now</a></li>
					</ul>
				</div>
				<div class="q_cat_sub2">
					<ul>
						<li><img src="product_img/qimg3.jpg"></li>
					</ul>
					<ul>
						<li><span>Intex Power Bank</span></li>
						<li><span>Upto 60% Discount</span></li>
						<li><a href="">Shop Now</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="clear"></div>

	<div class="quick_category">

		<div class="quick_cat_header">
			<ul>
				<li>MENS</li>
			</ul>
		</div>

		<div class="clear"></div>

		<div class="quick_cat_desc">
			<div class="quick_cat_menu">
				<ul>
					<li class="quick_menu_tittle">Top Categories</li>
					<?php foreach($men as $erow) { ?>
					<li><a href=""><?php echo $erow['name'] ?></a></li>
					<?php } ?>
				</ul>
			</div>
			<div class="quick_cat_items">
				<div class="q_cat_main">
					<ul>
						<li><img src="product_img/qimg1.jpg"></li>
					</ul>
					<ul>
						<li><span>New Arrivals</span></li>
						<li><a href="">Shop Now</a></li>
					</ul>
				</div>
				<div class="q_cat_sub">
					<ul>
						<li><img src="product_img/qimg2.jpg"></li>
					</ul>
					<ul>
						<li><span>Intex Power Bank</span></li>
						<li><span>Upto 60% Discount</span></li>
						<li><a href="">Shop Now</a></li>
					</ul>
				</div>
				<div class="q_cat_sub2">
					<ul>
						<li><img src="product_img/qimg3.jpg"></li>
					</ul>
					<ul>
						<li><span>Intex Power Bank</span></li>
						<li><span>Upto 60% Discount</span></li>
						<li><a href="">Shop Now</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="clear"></div>

