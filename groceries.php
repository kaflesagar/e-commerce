<?php require "include/header.php"; ?>
<?php require "include/menu.php"; ?>
<?php 

	if(isset($_GET['c'])){

		$id = $_GET['c'];

	} else{ 

		include "404.php";
		exit;
	} 
?>
<?php
	$and = '';

	if(isset($_GET['item'])){
		$item	= $_GET['item'];
		$and .= " and sub_category ='{$item}'";
	}

	if(isset($_GET['color'])){
		$color	= $_GET['color'];
		$and .= " and color ='{$color}'";
	}

	if(isset($_GET['price'])){
		$price	= $_GET['price'];
		$and .= " and price ='{$price}'";
	}

	if(isset($_GET['size'])){
		$size	= $_GET['size'];
		$and .= " and size ='{$size}'";
	}

	$get	= $obj->selectRequiredProductt("product","category_id","29",$and);
?>
<div>

	<div id="bread_crumb">Groceries / </div>

	<div id="filter_wrap">
		<div class="filterbox">
			<h2>Categories</h2>
			<ul>
				<li><input type="checkbox" name="cat" value="1"></li>
				<li>Tops</li>
			</ul>
			<ul>
				<li><input type="checkbox" name="cat" value="2"></li>
				<li>Dresses</li>
			</ul>
			<ul>
				<li><input type="checkbox" name="cat" value="3"></li>
				<li>Sweaters</li>
			</ul>
		</div>
		<div class="filterbox">
			<h2>Colors</h2>
			<div class="colorbox">
				<label><input type="checkbox" name="color"><span id="coral"></span></label>
				<label><input type="checkbox" name="color"><span id="crimson"></span></label>
				<label><input type="checkbox" name="color"><span id="aquamarine"></span></label>
				<label><input type="checkbox" name="color"><span id="olive"></span></label>
				<label><input type="checkbox" name="color"><span id="PaleGoldenRod"></span></label>
				<label><input type="checkbox" name="color"><span id="PaleGreen"></span></label>
				<label><input type="checkbox" name="color"><span id="PapayaWhip"></span></label>
				<label><input type="checkbox" name="color"><span id="PeachPuff"></span></label>
				<label><input type="checkbox" name="color"><span id="Pink"></span></label>
				<label><input type="checkbox" name="color"><span id="SeaShell"></span></label>
				<label><input type="checkbox" name="color"><span id="Snow"></span></label>
				<label><input type="checkbox" name="color"><span id="WhiteSmoke"></span></label>
				<label><input type="checkbox" name="color"><span id="Wheat"></span></label>
				<label><input type="checkbox" name="color"><span id="MistyRose"></span></label>
			</div>
		</div>

		<div class="filterbox">
			<h2>Price</h2>			
			<div id="slider-range"></div>
		    <input type="hidden" id="amount1">
		    <input type="hidden" id="amount2">
		    <p id="amount"></p>
		</div>

		<div class="filterbox">
			<h2>Size</h2>
			<ul>
				<li><input type="checkbox" name=""></li>
				<li>Small</li>
			</ul>
			<ul>
				<li><input type="checkbox" name=""></li>
				<li>Medium</li>
			</ul>
			<ul>
				<li><input type="checkbox" name=""></li>
				<li>Large</li>
			</ul>
			<ul>
				<li><input type="checkbox" name=""></li>
				<li>Extra Large</li>
			</ul>
		</div>

		<div class="filterbox">
			<h2>Discount</h2>
			<ul>
				<li><input type="checkbox" name=""></li>
				<li>50% Above</li>
			</ul>
			<ul>
				<li><input type="checkbox" name=""></li>
				<li>30% Above</li>
			</ul>
			<ul>
				<li><input type="checkbox" name=""></li>
				<li>10% Above</li>
			</ul>
		</div>

	</div>

	<div id="details_wrap">
		<div id="filter_holder">
			Showing : All Product
		</div>
		<div id="productlist">

			<?php foreach($get as $getr) {?>
			<div class="productbox">
				<ul>
					<a href="product?id=<?php echo $getr['product_id'] ?>">
					<li class="productimg"><img src="product_img/<?php echo $getr['photo1'] ?>"></li>
					</a>
					<li class="productname"><?php echo $getr['name'] ?></li>
					<li class="productprice">Price : <?php echo $getr['price'] ?></li>
				</ul>
				<div class="product_act">
					<ul>
						<li><a href="product?id=<?php echo $getr['product_id'] ?>">View Details</a></li>
						<li><a href="">Add to Card</a></li>
					</ul>
				</div>
			</div>
			<?php } ?>

		</div>
	</div>
</div>

<div class="clear"></div>

<?php require "include/footer.php"; ?>