<?php include "include/header.php"; ?>
<?php include "include/menu.php"; ?>
<div id="bread_crumb">Cart </div>
<div id="cart_wrap">
    <?php 
        if($_SESSION['cart'] == "") {
            echo "Not item avaible in Cart";
            die;
        } 
    ?>
    <?php

        //create blank to array to push cart value for paypal api use
        $apname     = array();
        $aqty       = array();
        $aamount    = array();
        $c = 0;
        $grandtotal = 0;
    ?>


    <?php foreach($_SESSION['cart'] as $row) { $c++ ?>

    <div id="cartdetails">
        <h2><?php echo $row['pname'] ?></h2>
        <ul>
            <li><img src="product_img/<?php echo $row['image'] ?>"></li>
        </ul>
        <ul>
            <li>Quantity</li>
            <li><?php echo $row['qty'] ?></li>
        </ul>
        <ul>
            <li>Size</li>
            <li><?php echo $row['size'] ?></li>
        </ul>
        <ul>
            <li>Color</li>
            <li><?php echo $row['color'] ?></li>
        </ul>
        <ul>
            <li>Amount</li>
            <li><?php echo $row['price'] ?></li>
        </ul>
        <ul>
            <li>Discount</li>
            <li><?php echo $row['discount'] ?>%</li>
        </ul>
        <?php $total = $row['price']*$row['qty']-($row['price']*$row['discount']/100); ?>
        <ul>
            <li>Total Amount</li>
            <li><?php echo $total ?></li>
        </ul>
        <?php 
            $grandtotal += $total; 
        ?>
    </div>
    <?php 

        //insert cart value to above array for paypal api use
        $nameString = '<input type="hidden" name="item_name_' . $c . '" value="' . $row['pname'] . '" />';
        $amountString = '<input type="hidden" name="amount_' . $c . '" value="' . $total . '" />';
        $quantityString = '<input type="hidden" name="quantity_' . $c . '" value="' . $row['qty'] . '" />';

        array_push($apname, $nameString);
        array_push($aqty, $quantityString);
        array_push($aamount, $amountString);
    ?>

    <?php } ?>

    <ul>
        <li>Grand Total : <?php echo $grandtotal; ?></li>
    </ul>

    <?php
        if (isset($_SESSION['email'])) {
        
    ?>
    <div>
        <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" name="_xclick">
              <input type="hidden" name="cmd" value="_cart" />
              <input type="hidden" name="upload" value="1" />
              <input type="hidden" name="business" value="seller@epeepal.com"/>
              <input type="hidden" name="currency_code" value="NPR" />

              <?php
              foreach ($apname as $value) {
                echo $value;
              }
              foreach ($aamount as $value1) {
                echo $value1;
              }
              foreach ($aqty as $value2) {
                echo $value2;
              }
               ?>

              <input type="hidden" name="return" value="http://localhost/epeepal_old/return_success" />
              <input type="hidden" name="cancel_return" value="http://localhost/epeepal_old/return_cancel" />
              <input type="submit" alt="Make payments with PayPal - it's fast, free and secure!" name="submit" value="Paypal" />
        </form>
    </div>
    <div>
        <form>
            <input type="submit" name="" value="MWallet">
        </form>
    </div>
    <div>
        <form>
            <input type="submit" name="" value="Credit Card">
        </form>
    </div>
    <?php } ?>
    <?php if(!isset($_SESSION['email'])) {?>
        <div>
            <a href="customerlogin">Checkout</a>
        </div>
    <?php } ?>
</div>
<?php include "include/footer.php"; ?>