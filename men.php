<?php require "include/header.php"; ?>
<?php require "include/menu.php"; ?>
<?php 

	if(isset($_GET['c'])){

		$id = $_GET['c'];

	} else{ 

		include "404.php";
		exit;
	} 
?>
<?php

	$id = 14;

	$and = '';

	if(isset($_GET['item'])){
		$item	= $_GET['item'];
		$and .= " and sub_category ='{$item}'";
	}

	$getp	= $obj->selectRequiredProductt("product","category_id",$id,$and);

	$getg 	= $obj->selectReqRowlimit("group_table","cat_id",$id,"name","0","4");
	//$sql = "SELECT * FROM $table WHERE $feild = $value ORDER BY $order ASC LIMIT $start,$end";
            
?>
<div>

	<div id="bread_crumb">Men / </div>
	<form id="search_form" action="">
	<div id="filter_wrap">
		<div class="filterbox">
			<h2>Categories</h2>
			<?php foreach($getg as $getr) {?>
			<ul>
				<li><input type="checkbox" name="cat[]" class="filter" value="<?php echo $getr['name'] ?>"></li>
				<li><?php echo $getr['name'] ?></li>
			</ul>
			<?php } ?>
		</div>
		<div class="filterbox">
			<h2>Colors</h2>
			<div class="colorbox">
				<label><input type="checkbox" class="filter" name="color[]" value="#SDFDFDd"><span id="coral"></span></label>
				<label><input type="checkbox" class="filter" name="color[]"><span id="crimson"></span></label>
				<label><input type="checkbox" class="filter" name="color[]"><span id="aquamarine"></span></label>
				<label><input type="checkbox" class="filter" name="color[]"><span id="olive"></span></label>
				<label><input type="checkbox" class="filter" name="color[]"><span id="PaleGoldenRod"></span></label>
				<label><input type="checkbox" class="filter" name="color[]"><span id="PaleGreen"></span></label>
				<label><input type="checkbox" class="filter" name="color[]"><span id="PapayaWhip"></span></label>
				<label><input type="checkbox" class="filter" name="color[]"><span id="PeachPuff"></span></label>
				<label><input type="checkbox" class="filter" name="color[]"><span id="Pink"></span></label>
				<label><input type="checkbox" class="filter" name="color[]"><span id="SeaShell"></span></label>
				<label><input type="checkbox" class="filter" name="color[]"><span id="Snow"></span></label>
				<label><input type="checkbox" class="filter" name="color[]"><span id="WhiteSmoke"></span></label>
				<label><input type="checkbox" class="filter" name="color[]"><span id="Wheat"></span></label>
				<label><input type="checkbox" class="filter" name="color[]"><span id="MistyRose"></span></label>
			</div>
		</div>

		<div class="filterbox">
			<h2>Price</h2>
			<!---		
			<div id="slider-range"></div>
		    <input type="hidden" id="amount1" class="filter" name="price[]">
		    <input type="hidden" id="amount2" class="filter" name="price[]">
		    <p id="amount"></p>		-->
		    <ul>
				<li><input type="checkbox" class="filter" name="price[]" value="1000"></li>
				<li>Below 1000</li>
			</ul>
			<ul>
				<li><input type="checkbox" class="filter" name="price[]" value="2000"></li>
				<li>Below 2000</li>
			</ul>
			<ul>
				<li><input type="checkbox" class="filter" name="price[]" value="5000"></li>
				<li>Below 5000</li>
			</ul>
			<ul>
				<li><input type="checkbox" class="filter" name="price[]" value="10000"></li>
				<li>Below 10000</li>
			</ul>
		</div>

		<div class="filterbox">
			<h2>Size</h2>
			<ul>
				<li><input type="checkbox" class="filter" name="size[]" value="EL"></li>
				<li>Extra Large</li>
			</ul>
			<ul>
				<li><input type="checkbox" class="filter" name="size[]" value="L"></li>
				<li>Large</li>
			</ul>
			<ul>
				<li><input type="checkbox" class="filter" name="size[]" value="M"></li>
				<li>Medium</li>
			</ul>
			<ul>
				<li><input type="checkbox" class="filter" name="size[]" value="S"></li>
				<li>Small</li>
			</ul>
		</div>

		<div class="filterbox">
			<h2>Discount</h2>
			<ul>
				<li><input type="checkbox" class="filter" name="discount[]" value="50"></li>
				<li>50% Above</li>
			</ul>
			<ul>
				<li><input type="checkbox" class="filter" name="discount[]" value="30"></li>
				<li>30% Above</li>
			</ul>
			<ul>
				<li><input type="checkbox" class="filter" name="discount[]" value="10"></li>
				<li>10% Above</li>
			</ul>
		</div>
	</div>
	</form>

	<div id="details_wrap">
		<div id="filter_holder">
			Showing : All Product
		</div>

		<div class="loader"></div>
		<div id="productlist">
			<?php foreach($getp as $getr) { 
				if(is_null($getr)){
    			echo "Product Not found"; die;
    			}  
				?>
			<div class="productbox">
				<ul>
					<a href="product?id=<?php echo $getr['product_id'] ?>">
					<li class="productimg"><img src="product_img/<?php echo $getr['photo1'] ?>"></li>
					</a>
					<li class="productname"><?php echo $getr['name'] ?></li>
					<li class="productprice">Price : <?php echo $getr['price'] ?></li>
				</ul>
				<div class="product_act">
					<ul>
						<li><a href="product?id=<?php echo $getr['product_id'] ?>">View Details</a></li>
					</ul>
				</div>
			</div>
			<?php } ?> 
		</div>
	</div>
</div>
<script type="text/javascript">

$('.loader').hide();

$('.filter').on('change', function() {
	
	var formData = $('#search_form').serialize();

  	$.ajax({
     type: "POST",
     url: "filter",
     data: formData,

    beforeSend: function(){
        $('.loader').show();
    },

    complete: function(){
        $('.loader').hide();
    },

    success: function(data){                  
        $('#productlist').html(data);
    }

   });
  return false;
});
</script>
<div class="clear"></div>
<div class="test">
</div>
<?php require "include/footer.php"; ?>