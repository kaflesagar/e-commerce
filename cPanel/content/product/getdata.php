<?php 
	require_once "../class/database.php";
	$obj = new Database();

	$cat_id = $_POST['cat_id'];
	$g = $obj->selectRequiredRow_Sub("group_table","cat_id",$cat_id,"name");

	$sub_id = $_POST['group_id'];
	$s = $obj->selectRequiredRow_Sub("sub_category","group_id",$sub_id,"name");

?>
<option value="">Select..</option>

<?php foreach($g as $r) {?>
	<option value="<?php echo $r['id'] ?>"><?php echo $r['name'] ?></option>
<?php } ?>

<?php foreach($s as $r) {?>
	<option value="<?php echo $r['id'] ?>"><?php echo $r['name'] ?></option>
<?php } ?>