<?php

$start  = 0;
$end    = 20;

if(isset($_GET['pagenum'])){
    $id = $_GET['pagenum'];
    $start = ($id-1)*$end;
}

$pagelimit   = $obj -> pagination("product","name",$start,$end);
$totalcount  = $obj -> pageCount("product",$end);

?>

<div class="page_tittle"><?php if (isset($_GET['page'])); echo $_GET['page']; ?></div>

    <div class="clear"></div>
    <p class="flag"><?php if(isset($_GET['flag'])) { ?> <?php echo $_GET['flag'] ?> <?php } ?></p>
    <form method="POST" action="product/process">
    <input type="hidden" name="opeation" value="delete" />

    <div class="loop_table">
    
        <div class="top_action">
            <ul>
                <li><a href="?page=add_product">Add New Product</a></li>     
            </ul>
        </div>
        
        <div class="clear"></div>
        
        <table>  
            <tr>
                <th>SN</th>
                <th>Product Code</th>
                <th>Product Name</th>
                <th>Product Image</th>
                <th>Category </th>
                <th>Group </th>
                <th>Sub Category </th>
                <th>Price</th>
                <th>Stock</th>
                <th>Details</th>
                <th></th>
                <th></th>
            </tr>
            
            <?php $i=1; foreach ($pagelimit as $list) { ?>
            
            <tr>
                <td><?php echo $i++; ?></td>
                <td><?php echo $list['product_code']; ?></td>
                <td><?php echo $list['name']; ?></td>
                <td><img src="../../product_img/<?php echo $list['photo1'] ?>" width="40px" height="40px" /></td>
                <td><?php echo $list['category']; ?></td>
                <td><?php echo $list['group_name']; ?></td>
                <td><?php echo $list['sub_category']; ?></td>
                <td><?php echo $list['price']; ?></td>
                <td><?php echo $list['stock']; ?></td>
                <td><a href="?page=product_details&cid=<?php echo $list['category_id'] ?>&gid=<?php echo $list['group_id'] ?>&sid=<?php echo $list['sub_category_id'] ?>&pid=<?php echo $list['product_id'] ?>">View</a></td>
                <td></td>
                <td>
                    <a href="?page=edit_product&cid=<?php echo $list['category_id'] ?>&gid=<?php echo $list['group_id'] ?>&sid=<?php echo $list['sub_category_id'] ?>&pid=<?php echo $list['product_id'] ?>">Edit</a>
                    <a href="javascript:void(0);" onclick='deleteproduct("<?php echo $list['product_id']; ?>");'>Delete</a>
                </td>
            </tr>

            <?php } ?>

        </table>

        <p class="page_number">
            <?php for($i = 1; $i <= $totalcount; $i++){ ?>
            <a href="?page=product&pagenum=<?php echo $i; ?>">
                <?php echo $i; ?>
            </a>
            <?php } ?>
        </p>

    </div>
    </form>

    <script type="text/javascript">
    
    function deleteproduct(argu){
        var ok = confirm("Are you sure want to delete?");
        if(ok){
            window.location = "product/process?operation=delete&id="+argu;
        }
    }

    </script>