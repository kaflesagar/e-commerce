<?php 
    $c = $obj->selectAll("category","name");
    $s = $obj->selectAll("sub_category","name");
?>
<script type="text/javascript">
    function getGroup(val) {
        $.ajax({
            type: "POST",
            url: "product/getdata",
            data:'cat_id='+val,
            success: function(data){
                //alert("test");
                $(".group_list").html(data);
            }
        });
    }

    function getsubCat(val) {
        $.ajax({
            type: "POST",
            url: "product/getdata",
            data:'group_id='+val,
            success: function(data){
                //alert("test");
                $(".sub_cat_list").html(data);
            }
        });
    }
</script>
<div class="page_tittle">Product >> Add New Product</div>

<div class="add_wrap">
    <form method="POST" action="product/process" enctype="multipart/form-data">
    <input type="hidden" name="operation" value="add" />
    <input type="hidden" name="product_code" />
    <input type="hidden" name="entry_date" value="<?php date_default_timezone_set("Asia/Kathmandu"); echo date("d m Y h:i:s"); ?>" />
        <table>        
            <tr>
                <td>Category</td>
                <td> : </td>
                <td>
                    <select name="category_id" onChange="getGroup(this.value)" required="">
                        <option value="">Select...</option>
                        <?php foreach ($c as $cat) { ?>
                        <option value="<?php echo $cat['category_id']; ?>"><?php echo $cat['name'] ?></option>
                        <?php } ?>
                    </select>
                </td>    
            </tr>

            <tr>
                <td>Group</td>
                <td> : </td>
                <td>
                    <select name="gid" class="group_list" onChange="getsubCat(this.value)" required="">
                        <option value="">Select ...</option>
                    </select>
                </td>    
            </tr>

			<tr>
                <td>Sub Category</td>
                <td> : </td>
                <td>
                    <select name="sub_category_id" class="sub_cat_list" required="">
                        <option value="">Select ...</option>
                    </select>
                </td>    
            </tr>

            <tr>
                <td width="160">Product Name</td>
                <td width="10"> : </td>
                <td><input type="text" name="product_name" required="required" /></td>    
            </tr>

            <tr>
                <td>Image 1 (Main Image)</td>
                <td> : </td>
                <td><input type="file" name="product_img_1" required="" /></td>
            </tr>

            <tr>
                <td>Image 2</td>
                <td> : </td>
                <td><input type="file" name="product_img_2" required="" /></td>
            </tr>

            <tr>
                <td>Image 3</td>
                <td> : </td>
                <td><input type="file" name="product_img_3" required="" /></td>
            </tr>

            <tr>
                <td>Image 4</td>
                <td> : </td>
                <td><input type="file" name="product_img_4" required="" /></td>
            </tr>

            <tr>
                <td>Price (NPR)</td>
                <td> : </td>
                <td><input type="text" name="product_price" onkeypress='return isNumberKey(event)' required="" /></td>    
            </tr>

            <tr>
                <td>Discount (%)</td>
                <td> : </td>
                <td><input type="text" name="discount" onkeypress='return isNumberKey(event)' required="" /></td>    
            </tr>

			<tr>
                <td>Color</td>
                <td> : </td>
                <td><input type="text" name="color" required="" /></td>    
            </tr>
                               
            <tr class="cbox">
                <td>Size</td>
                <td> : </td>
                <td>
                    <label><input type="checkbox" name="size1"><span>EL</span></label>
                    <label><input type="checkbox" name="size2"><span>L</span></label>
                    <label><input type="checkbox" name="size3"><span>M</span></label>
                    <label><input type="checkbox" name="size4"><span>S</span></label>
                    <label><input type="checkbox" name=""><span>N/A</span></label>
                </td>    
            </tr>

            <tr>
                <td></td>
                <td></td>
                <td><i>EL = Extra Large, L = Large, M = Medium, S = Small, N/A = Not Applicable</i></td>
            </tr>

            <tr>
                <td>Stock</td>
                <td> : </td>
                <td><input type="text" name="stock" required="" /></td>
            </tr>

            <tr>
                <td>Product Caption</td>
                <td> : </td>
                <td><textarea name="product_caption" cols="60" rows="6" required></textarea></td>    
            </tr>

            <tr>
                <td>Material & Care</td>
                <td> : </td>
                <td><textarea name="material_care" cols="60" rows="5" required></textarea></td>    
            </tr>

            <tr>
                <td>Product Description</td>
                <td> : </td>
                <td><textarea name="product_desc" cols="60" rows="10" resized="disabled" required></textarea></td>    
            </tr>

            <tr>
                <td>Other Details</td>
                <td> : </td>
                <td><textarea name="other_details" cols="60" rows="5" required></textarea></td>    
            </tr>
            
        </table>
    
        <div><input type="submit" name="submit" value="Add Product" class="add_btn" /></div> 
        
    </form>    
</div>