<?php 
    $cid = $_GET['cid'];
    $gid = $_GET['gid'];
    $sid = $_GET['sid'];
    $pid = $_GET['pid'];

    $p = $obj -> selectRequiredRow("product","product_id",$pid);      //fetch Product Name
    $c = $obj -> selectRequiredRow("category","category_id",$cid);    //fetch Category
    $g = $obj -> selectRequiredRow("group_table","id",$gid);          //fetch gropup
    $s = $obj -> selectRequiredRow("sub_category","id",$sid);         //fetch Sub Category
    $call = $obj -> selectAll("category","name");                     //fetch all category for drop down list
?>
<script type="text/javascript">
    function getGroup(val) {
        $.ajax({
            type: "POST",
            url: "product/getdata",
            data:'cat_id='+val,
            success: function(data){
                //alert("test");
                $(".group_list").html(data);
            }
        });
    }

    function getsubCat(val) {
        $.ajax({
            type: "POST",
            url: "product/getdata",
            data:'group_id='+val,
            success: function(data){
                //alert("test");
                $(".sub_cat_list").html(data);
            }
        });
    }
</script>
<div class="page_tittle">Product >> Edit Product</div>

<div class="add_wrap">
    <form method="POST" action="product/process" enctype="multipart/form-data">
    <input type="hidden" name="operation" value="edit" />
    <input type="hidden" name="pid" value="<?php echo $pid; ?>" />
    <input type="hidden" name="product_img_1" value="<?php echo $p['photo1'] ?>"/>
    <input type="hidden" name="product_img_2" value="<?php echo $p['photo2'] ?>"/>
    <input type="hidden" name="product_img_3" value="<?php echo $p['photo3'] ?>"/>
    <input type="hidden" name="product_code" value="<?php echo $p['product_code']; ?>" />

        <table>       
        
            <tr>
                <td width="160">Category</td>
                <td width="10"> : </td>
                <td>
                    <select name="category_id" onChange="getGroup(this.value)" required="">
                        <option value="<?php echo $c['category_id'] ?>"><?php echo $c['name'] ?></option>
                        <?php foreach ($call as $cat) { ?>
                        <option value="<?php echo $cat['category_id']; ?>"><?php echo $cat['name'] ?></option>
                        <?php } ?>
                    </select>
                </td>    
            </tr>

            <tr>
                <td width="160">Group</td>
                <td width="10"> : </td>
                <td>
                    <select name="gid" class="group_list" onChange="getsubCat(this.value)" required="">
                        <option value="<?php echo $g['id'] ?>"><?php echo $g['name'] ?></option>
                    </select>
                </td>
            </tr>

            <tr>
                <td width="160">Sub Category</td>
                <td width="10"> : </td>
                <td>
                    <select name="sub_category_id" class="sub_cat_list" required="">
                        <option value="<?php echo $s['id'] ?>"><?php echo $s['name'] ?></option>
                    </select>
                </td>
            </tr>

            <tr>
                <td width="160">Display Image 1</td>
                <td width="10"> : </td>
                <td><input type="file" name="product_img_1" value="<?php echo $p['photo1'] ?>" /></td>
            </tr>

            <tr>
                <td width="160">Display Image 2</td>
                <td width="10"> : </td>
                <td><input type="file" name="product_img_2" value="<?php echo $p['photo2'] ?>" /></td>
            </tr>

            <tr>
                <td width="160">Display Image 3</td>
                <td width="10"> : </td>
                <td><input type="file" name="product_img_3" value="<?php echo $p['photo3'] ?>" /></td>
            </tr>

            <tr>
                <td width="160">Display Image 4</td>
                <td width="10"> : </td>
                <td><input type="file" name="product_img_4" value="<?php echo $p['photo4'] ?>" /></td>
            </tr>

            <tr>
                <td width="160">Product Name</td>
                <td width="10"> : </td>
                <td><input type="text" name="product_name" required="" value="<?php echo $p['name'] ?>" /></td>
            </tr>

            <tr>
                <td>Price (NPR)</td>
                <td> : </td>
                <td><input type="text" name="product_price" onkeypress='return isNumberKey(event)' value="<?php echo $p['price'] ?>" required="" /></td>    
            </tr>

            <tr>
                <td>Discount (%)</td>
                <td> : </td>
                <td><input type="text" name="discount" onkeypress='return isNumberKey(event)' value="<?php echo $p['discount'] ?>" required="" /></td>    
            </tr>

            <tr>
                <td>Color</td>
                <td> : </td>
                <td><input type="text" name="color" value="<?php echo $p['color'] ?>" required="" /></td>    
            </tr>
                               
            <tr>
                <td>Size</td>
                <td> : </td>
                <td>
                    <select name="size" required>
                        <option value="<?php echo $p['size'] ?>"><?php echo $p['size'] ?></option>
                        <option value="el">EL</option>
                        <option value="l">L</option>
                        <option value="m">M</option>
                        <option value="s">S</option>
                        <option value="s">N/A</option>
                    </select>
                </td>    
            </tr>

            <tr>
                <td></td>
                <td></td>
                <td><i>EL = Extra Large, L = Large, M = Medium, S = Small, N/A = Not Applicable</i></td>
            </tr>

            <tr>
                <td>Stock</td>
                <td> : </td>
                <td><input type="text" name="stock" value="<?php echo $p['stock'] ?>" required="" /></td>
            </tr>

            <tr>
                <td>Product Caption</td>
                <td> : </td>
                <td><textarea name="product_caption" cols="60" rows="6" required><?php echo $p['product_caption'] ?></textarea></td>    
            </tr>

            <tr>
                <td>Material & Care</td>
                <td> : </td>
                <td><textarea name="material_care" cols="60" rows="5" required><?php echo $p['material_care'] ?></textarea></td>    
            </tr>

            <tr>
                <td>Product Description</td>
                <td> : </td>
                <td><textarea name="product_desc" cols="60" rows="10" required><?php echo $p['product_desc'] ?></textarea></td>    
            </tr>

            <tr>
                <td>Other Details</td>
                <td> : </td>
                <td><textarea name="other_details" cols="60" rows="5" required><?php echo $p['other_details'] ?></textarea></td>    
            </tr>
            
        </table>
    
        <div><input type="submit" name="submit" value="Confirm" class="add_btn" /></div> 
        
    </form>    
</div>