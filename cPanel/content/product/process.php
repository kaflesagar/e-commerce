<?php

require "../class/database.php";
$obj = new Database();
$operation_type = null;

    if($_REQUEST['operation']){
        
        $operation_type = $_REQUEST['operation'];
        
    } 
    
    switch($operation_type){
        
        case 'add':
            if($_REQUEST['product_code']==''){
            $product_code = mt_rand(0000000000,9999999999);
            }
            
            $filename1   = $_FILES['product_img_1']['name'];
            $random1     = rand(000000000,999999999);
            $filename1   = $random1.'-'.$filename1;
            $temp1       = $_FILES['product_img_1']['tmp_name'];
            $per1        = '../../../product_img/'.$filename1;
            move_uploaded_file($temp1,$per1);

            $filename2   = $_FILES['product_img_2']['name'];
            $random2     = rand(000000000,999999999);
            $filename2   = $random2.'-'.$filename2;
            $temp2       = $_FILES['product_img_2']['tmp_name'];
            $per2        = '../../../product_img/'.$filename2;
            move_uploaded_file($temp2,$per2);

            $filename3   = $_FILES['product_img_3']['name'];
            $random3     = rand(000000000,999999999);
            $filename3   = $random3.'-'.$filename3;
            $temp3       = $_FILES['product_img_3']['tmp_name'];
            $per3        = '../../../product_img/'.$filename3;
            move_uploaded_file($temp3,$per3);

            $filename4   = $_FILES['product_img_4']['name'];
            $random4     = rand(000000000,999999999);
            $filename4   = $random4.'-'.$filename4;
            $temp4       = $_FILES['product_img_4']['tmp_name'];
            $per4        = '../../../product_img/'.$filename4;
            move_uploaded_file($temp4,$per4);


            if($g_id= $_REQUEST['gid']){
                $g_id = $obj->selectRequiredRow("group_table","id",$g_id);
                $group_name = $g_id['name'];
            }

            if($cat= $_REQUEST['category_id']){
                $cat = $obj->selectRequiredRow("category","category_id",$cat);
                $category = $cat['name'];
            }

            if($sub_cat= $_REQUEST['sub_category_id']){
                $sub_cat = $obj->selectRequiredRow("sub_category","id",$sub_cat);
                $sub_category = $sub_cat['name'];
            }

            $size1 = mysql_real_escape_string($_REQUEST['size1']);
            $size2 = mysql_real_escape_string($_REQUEST['size2']);
            $size3 = mysql_real_escape_string($_REQUEST['size3']);
            $size4 = mysql_real_escape_string($_REQUEST['size4']);

            if($size1 == "on"){
                $size1 = "EL";
            }

            if($size2 == "on"){
                $size2 = "L";
            }

            if($size3 == "on"){
                $size3 = "M";
            }

            if($size4 == "on"){
                $size4 = "S";
            }

            $data = array (
            'product_code'      => mysql_real_escape_string($product_code),
            'category'          => mysql_real_escape_string($category),
            'category_id'       => mysql_real_escape_string($_REQUEST['category_id']),
            'group_name'        => mysql_real_escape_string($group_name),
            'group_id'          => mysql_real_escape_string($_REQUEST['gid']),
            'sub_category'      => mysql_real_escape_string($sub_category),
            'sub_category_id'   => mysql_real_escape_string($_REQUEST['sub_category_id']),
            'name'              => mysql_real_escape_string($_REQUEST['product_name']),
            'photo1'            => mysql_real_escape_string($filename1),
            'photo2'            => mysql_real_escape_string($filename2),
            'photo3'            => mysql_real_escape_string($filename3),
            'photo4'            => mysql_real_escape_string($filename4),
            'price'             => mysql_real_escape_string($_REQUEST['product_price']),
            'discount'          => mysql_real_escape_string($_REQUEST['discount']),
            'color'             => mysql_real_escape_string($_REQUEST['color']),
            'size1'             => $size1,
            'size2'             => $size2,
            'size3'             => $size3,
            'size4'             => $size4,
            'stock'             => mysql_real_escape_string($_REQUEST['stock']),
            'product_caption'   => mysql_real_escape_string($_REQUEST['product_caption']),
            'material_care'     => mysql_real_escape_string($_REQUEST['material_care']),
            'product_desc'      => mysql_real_escape_string($_REQUEST['product_desc']),
            'other_details'     => mysql_real_escape_string($_REQUEST['other_details']),
            'entry_date'        => mysql_real_escape_string($_REQUEST['entry_date'])
            );
            //print_r($data); die();
            $obj->insert("product",$data);
            header("location:../?page=product&flag=Product Added Successfully");
            break;
            
        case 'edit':
            $id         = $_REQUEST['pid'];
            $gid        = $_REQUEST['gid'];
            
            $filename1  = $_FILES['product_img_1']['name'];
            $actulname1 = $_POST['product_img_1'];
            $filename1  = $actulname1;
            $temp1      = $_FILES['product_img_1']['tmp_name'];
            $per1       = '../../../product_img/'.$filename1;
            move_uploaded_file($temp1,$per1);

            $filename2  = $_FILES['product_img_2']['name'];
            $actulname2 = $_POST['product_img_2'];
            $filename2  = $actulname2;
            $temp2      = $_FILES['product_img_2']['tmp_name'];
            $per2       = '../../../product_img/'.$filename2;
            move_uploaded_file($temp2,$per2);

            $filename3  = $_FILES['product_img_3']['name'];
            $actulname3 = $_POST['product_img_3'];
            $filename3  = $actulname3;
            $temp3      = $_FILES['product_img_3']['tmp_name'];
            $per3       = '../../../product_img/'.$filename3;
            move_uploaded_file($temp3,$per3);

            $filename4   = $_FILES['product_img_4']['name'];
            $random4     = rand(000000000,999999999);
            $filename4   = $random4.'-'.$filename4;
            $temp4       = $_FILES['product_img_4']['tmp_name'];
            $per4        = '../../../product_img/'.$filename4;
            move_uploaded_file($temp4,$per4);

            if($g_id= $_REQUEST['gid']){
                $g_id = $obj->selectRequiredRow("group_table","id",$g_id);
                $group_name = $g_id['name'];
            }

            if($cat= $_REQUEST['category_id']){
                $cat = $obj->selectRequiredRow("category","category_id",$cat);
                $category = $cat['name'];
            }

            if($sub_cat= $_REQUEST['sub_category_id']){
                $sub_cat = $obj->selectRequiredRow("sub_category","id",$sub_cat);
                $sub_category = $sub_cat['name'];
            }

            $data = array (
            'category'          => mysql_real_escape_string($category),
            'category_id'       => mysql_real_escape_string($_REQUEST['category_id']),
            'group_name'        => mysql_real_escape_string($group_name),
            'group_id'          => mysql_real_escape_string($_REQUEST['gid']),
            'sub_category'      => mysql_real_escape_string($sub_category),
            'sub_category_id'   => mysql_real_escape_string($_REQUEST['sub_category_id']),
            'name'              => mysql_real_escape_string($_REQUEST['product_name']),
            'photo1'            => mysql_real_escape_string($filename1),
            'photo2'            => mysql_real_escape_string($filename2),
            'photo3'            => mysql_real_escape_string($filename3),
            'photo4'            => mysql_real_escape_string($filename4),
            'price'             => mysql_real_escape_string($_REQUEST['product_price']),
            'discount'          => mysql_real_escape_string($_REQUEST['discount']),
            'color'             => mysql_real_escape_string($_REQUEST['color']),
            'size'              => mysql_real_escape_string($_REQUEST['size']),
            'stock'             => mysql_real_escape_string($_REQUEST['stock']),
            'product_caption'   => mysql_real_escape_string($_REQUEST['product_caption']),
            'material_care'     => mysql_real_escape_string($_REQUEST['material_care']),
            'product_desc'      => mysql_real_escape_string($_REQUEST['product_desc']),
            'other_details'     => mysql_real_escape_string($_REQUEST['other_details'])
            );

            //print_r($data); die();
            $obj->editRow("product",$data,"product_id", $id);
            header("location:../?page=product&flag=Product updated successfully");
            break;
                    
        case 'delete':
            $id = $_REQUEST['id'];         

            $img = $obj->selectRequiredRow("product","product_id",$id);

            $p1 = '../../../product_img/'.$img['photo'];
            $p2 = '../../../product_img/'.$img['photo2'];
            $p3 = '../../../product_img/'.$img['photo3']; 
            unlink($p1); unlink($p2); unlink($p3);          

            $obj->deleteRecord("product","product_id",$id);   

            header ("location:../?page=product&flag=Product deleted successfully !");
            break;
            
        case 'delete_checbox':
            $id = $_REQUEST['product_id'];
            $checkbox = $_REQUEST['checkbox'];
            $id = implode(',',$checkbox);
            


            $img = $obj->selectRequiredRow("product","product_id",$id);
            $p1 = '../../../product_img/'.$img['photo'];
            $p2 = '../../../product_img/'.$img['photo2'];
            $p3 = '../../../product_img/'.$img['photo3'];            

            unlink($p1); unlink($p2); unlink($p3);

            $obj->deleteSelectedRecord("product","product_id",$id); 
            
            header("location:../?page=product&flag=Category deleted successfully"); 
            break;
               
        default :
        header("location:../");
        
    }

?>