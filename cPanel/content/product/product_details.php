<?php

$cid = $_GET['cid'];
$gid = $_GET['gid'];
$sid = $_GET['sid'];
$pid = $_GET['pid'];

if($_GET['cid']) {
    $cat_id = $obj ->selectRequiredRow("product","product_id",$pid);
    $cid = $cat_id['category_id'];
}
$result1 = $obj -> selectRequiredRow("product","product_id",$pid);      //fetch Product 
$result2 = $obj -> selectRequiredRow("category","category_id",$cid);    //fetch Category
$result7 = $obj -> selectRequiredRow("group_table","id",$gid);          //fetch Category
$result4 = $obj -> selectRequiredRow("sub_category","id",$sid);         //fetch Sub Category
$result3 = $obj -> selectAll("category","name");                        //fetch all category for drop down list
$result5 = $obj -> selectAll("sub_category","name");                    //fetch all sub category for drop down list
$result6 = $obj -> selectRequiredRow("product","product_id",$pid);      //fetch all details of product, like price, image, details, status
?>
<script>
    function change(sant){
        abc = document.getElementById("effect");
        abc.src = sant;
    }
</script>
<div class="page_tittle">Product >> Product Details</div>
    <div class="p_details_left">
            <div class="p_image">
                <img src="../../product_img/<?php echo $result1['photo1']; ?>" id="effect"/>
            </div>

            <div class="img_thumble">
                <ul>
                    <li><img src="../../product_img/<?php echo $result1['photo1']; ?>" onclick="change('../../product_img/<?php echo $result1['photo1']; ?>')" />   </li>
                    <li><img src="../../product_img/<?php echo $result1['photo2']; ?>" onclick="change('../../product_img/<?php echo $result1['photo2']; ?>')"/>  </li>
                    <li><img src="../../product_img/<?php echo $result1['photo3']; ?>" onclick="change('../../product_img/<?php echo $result1['photo3']; ?>')"/>  </li>
                    <li><img src="../../product_img/<?php echo $result1['photo4']; ?>" onclick="change('../../product_img/<?php echo $result1['photo4']; ?>')"/>  </li>
                </ul>
            </div>
    </div>

    <div class="p_details_right">
        <table>

            <tr>
                <td width="160">Product Name</td>
                <td width="10"> : </td>
                <td><?php echo $result1['name'] ?></td>
            </tr>

            <tr>
                <td width="160">Category</td>
                <td width="10"> : </td>
                <td><?php echo $result2['name'] ?></td>
            </tr>

            <tr>
                <td width="160">Group</td>
                <td width="10"> : </td>
                <td><?php echo $result7['name'] ?></td>
            </tr>

            <tr>
                <td width="160">Sub Category</td>
                <td width="10"> : </td>
                <td><?php echo $result4['name']; ?></td>
            </tr>

            <tr>
                <td width="160">Price</td>
                <td width="10"> : </td>
                <td><?php echo $result6['price'] ?></td>
            </tr>

            <tr>
                <td>Size</td>
                <td> : </td>
                <td><?php echo $result6['size1'] ?> | <?php echo $result6['size2'] ?> | <?php echo $result6['size3'] ?> | <?php echo $result6['size4'] ?></td>
            </tr>

            <tr>
                <td>color</td>
                <td> : </td>
                <td><?php echo $result6['color'] ?></td>
            </tr>

            <tr>
                <td width="160">Stock</td>
                <td width="10"> : </td>
                <td><?php echo $result6['stock'] ?></td>
            </tr>

            <tr>
                <td width="160">product_caption</td>
                <td width="10"> : </td>
                <td class="view"><textarea id="product_desc" cols="40" rows="5" disabled="disabled"><?php echo $result6['product_caption'] ?></textarea></td>
            </tr>

            <tr>
                <td width="160">material_care</td>
                <td width="10"> : </td>
                <td class="view"><textarea id="product_desc" cols="40" rows="5" disabled="disabled"><?php echo $result6['material_care'] ?></textarea></td>
            </tr>

            <tr>
                <td width="160">product_desc</td>
                <td width="10"> : </td>
                <td class="view"><textarea id="product_desc" cols="40" rows="5" disabled="disabled"><?php echo $result6['product_desc'] ?></textarea></td>
            </tr>

            <tr>
                <td width="160">other_details</td>
                <td width="10"> : </td>
                <td class="view"><textarea id="product_desc" cols="40" rows="5" disabled="disabled"><?php echo $result6['other_details'] ?></textarea></td>
            </tr>

            <tr>
                <td>Product Added Date</td>
                <td> : </td>
                <td><?php echo $result6['entry_date']; ?></td>
            </tr>

            <tr>
                <td>Product Code</td>
                <td> : </td>
                <td><?php echo $result6['product_code']; ?></td>
            </tr>

        </table>

        <div class="goback">
            <button onclick="history.back(-1);">Go Back</button>
        </div>

    </div>
    <div class="clear"></div>
<script>
    var s_height = document.getElementById('product_desc').scrollHeight;
    document.getElementById('product_desc').setAttribute('style','height:'+s_height+'px');
</script>