<?php

$result = $obj->selectAll("customer","first_name");

if(isset($_POST['checkbox'])){
    
    $checkbox = $_POST['checkbox'];
    $id = implode(',',$checkbox);
    
    $obj->deleteSelectedRecord("customer","customer_id",$id); 
    header("location:index.php?page=customer&flag=Category deleted successfully");
}
?>
<div class="page_tittle"><?php if (isset($_GET['page'])); echo $_GET['page']; ?></div>
    
    <div class="clear"></div>
    <form method="POST" action="">
    <div class="loop_table">

        <div class="top_action">
            <ul>
                <li><a href="">Import</a></li>
                <li><a href="">Export</a></li>
                <li><a href="">Delete</a></li>
            </ul>
        </div>

        <div class="clear"></div>

        <table>
            
            <tr>
                <th>SN</th>
                <th>Cu. ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Mobile</th>
                <th>Country</th>
                <th>Zone</th>
                <th>District</th>
                <th>Full Address</th>
                <th>Enter Date</th>
                <th>Offers</th>
                <th>Select</th>              
            </tr>
            
            <?php $i=1; foreach ($result as $list) { ?>
            
            <tr>
                <td><?php echo $i++; ?></td>
                <td><?php echo $list['customer_id']?></td>
                <td><?php echo $list['first_name']?></td>
                <td><?php echo $list['last_name']?></td>
                <td><?php echo $list['email']?></td>
                <td><?php echo $list['mobile']?></td>
                <td><?php echo $list['country']?></td>
                <td><?php echo $list['zone']?></td>
                <td><?php echo $list['district']?></td>
                <td><?php echo $list['full_address']?></td>
                <td><?php echo $list['enter_date']?></td>
                <td><?php echo $list['exclusive_offer']?></td>
                <td><input type="checkbox" name="checkbox[]" value="<?php echo $list['customer_id']?>" /></td>                
            </tr>
            
            <?php } ?>
        
        </table>
    </div>
    </form>
