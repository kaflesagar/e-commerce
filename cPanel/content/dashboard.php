<div class="page_tittle">Dashboard</div>
    
<div class="clear"></div>

        <div class="overview">
            <p>Oder Overview</p>
            <div class="overview_table">
                <table>
                    <tr class="overview_head">
                        <th width="100">Order Status</th>
                        <th>Today</th>
                        <th>This Week</th>
                    </tr>
                    
                    <tr>
                        <td>Completed</td>
                        <td></td>
                        <td></td>
                    </tr>
                    
                    <tr class="overview_light">
                        <td>Pending</td>
                        <td></td>
                        <td></td>
                    </tr>
                                        
                     <tr>
                        <td>Cancelled</td>
                        <td></td>
                        <td></td>
                    </tr>
                    
                    <tr class="overview_bottom">
                        <th colspan="3" class="refresh"><a href="">Refresh</a></th>
                    </tr>                   
                    
                </table>
            </div>
        </div>
        
        <div class="overview">
            <p>Highest Sales Items</p>
            <div class="overview_table">
                <table>
                    <tr class="overview_head">
                        <th width="100">Items</th>
                        <th>Today's Count</th>
                        <th>This Week's Count</th>
                    </tr>
                    
                    <tr>
                        <td>iPhone 6</td>
                        <td></td>
                        <td></td>
                    </tr>
                    
                    <tr class="overview_light">
                        <td>Macbook</td>
                        <td></td>
                        <td></td>
                    </tr>
                                        
                     <tr>
                        <td>Glass - RayBan</td>
                        <td></td>
                        <td></td>
                    </tr>   
                    
                    <tr class="overview_bottom">
                        <th colspan="3" class="refresh"><a href="">Refresh</a></th>
                    </tr>                 
                    
                </table>
            </div>
        </div>
        
        <div class="overview">
            <p>Popular Items</p>
            <div class="overview_table">
                <table>
                    <tr class="overview_head">
                        <th width="100">Items</th>
                        <th>Today's Count</th>
                        <th>This Week's Count</th>
                    </tr>
                    
                    <tr>
                        <td>Google Nexus 6</td>
                        <td></td>
                        <td></td>
                    </tr>
                    
                    <tr class="overview_light">
                        <td>Galaxy S6</td>
                        <td></td>
                        <td></td>
                    </tr>
                                        
                     <tr>
                        <td>iPhone 4s</td>
                        <td></td>
                        <td></td>
                    </tr>   
                    
                    <tr class="overview_bottom">
                        <th colspan="3" class="refresh"><a href="">Refresh</a></th>
                    </tr>                 
                    
                </table>
            </div>
        </div>
        
        <div class="overview">
            <p>Register Customer</p>
            <div class="overview_table">
                <table>
                    <tr class="overview_head">
                        <th width="150">Period</th>
                        <th>Total Count</th>
                    </tr>
                    
                    <tr>
                        <td>In Today</td>
                        <td></td>
                    </tr>
                    
                    <tr class="overview_light">
                        <td>In 7 days</td>
                        <td></td>
                    </tr>
                                        
                     <tr>
                        <td>In a last month</td>
                        <td></td>
                    </tr>   
                    
                    <tr class="overview_bottom">
                        <th colspan="2" class="refresh"><a href="">Refresh</a></th>
                    </tr>                 
                    
                </table>
            </div>
        </div>