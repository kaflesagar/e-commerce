<?php include 'header.php'; ?>
<?php include 'menu.php'; ?>
<div class="body_wrap">
    <?php
    	if(isset($_GET['page']))
            {     	   
        		$page = $_GET['page']; 
                   		
        		if($page == "dashboard")            {include "dashboard.php";}
                
                //Search
                elseif($page == "search")           {include "search.php";}
                
                //User Menu
                elseif($page == "user")             {include "user/user.php";}
                elseif($page == "add_user")         {include "user/add_user.php";}
                elseif($page == "edit_user")        {include "user/edit_user.php";}
                elseif($page == "import_user")      {include "user/import_user.php";}
                
                //Product Menu
                elseif($page == "product")          {include "product/product.php";}
                elseif($page == "add_product")      {include "product/add_product.php";}
                elseif($page == "edit_product")     {include "product/edit_product.php";}
                elseif($page == "product_details")  {include "product/product_details.php";}

                //Category Menu
                elseif($page == "category")         {include "category/category.php";}
                elseif($page == "add_category")     {include "category/add_category.php";}
                elseif($page == "edit_category")    {include "category/edit_category.php";}

                elseif($page == "product_add")      {include "category/product_add.php";}
                elseif($page == "view_product")     {include "category/view_product.php";}

                elseif($page == "group")            {include "category/group.php";}
                elseif($page == "add_group")        {include "category/add_group.php";}
                elseif($page == "edit_group")       {include "category/edit_group.php";}

				elseif($page == "sub_category") 	{include "category/sub_category.php";}
				elseif($page == "add_sub_category")	{include "category/add_sub_category.php";}
                elseif($page == "edit_sub_category"){include "category/edit_sub_category.php";}
				
                //Cusotmer Menu
                elseif($page == "customer")         {include "customer/customer.php";}
                
                //Orders Menu
                elseif($page == "orders")           {include "orders/orders.php";}

                //Search
                elseif($page == "search")           {include "search.php";}
                
                //404 Error menu           
                else {include "404.php";}
            } 
    		else {include "dashboard.php";}    	
    ?>
</div>
