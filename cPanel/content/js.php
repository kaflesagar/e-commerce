
<script type="text/javascript">
/*
    /////////// alert
    alert ("hello world");
*/

/*
    /////////// confirm
    var cc = confirm("hello");		// var is use to define variable in js

    if(cc){
        alert ("you clicked ok");
    } else
        alert ("you clicked cancel");
*/

/*
    /////////// prompt
    var ans = prompt("Inter Your Name");
        if(ans){
            alert (ans);
        }
*/

/*
    /////////// combine variable
    var name = "Santosh";
    alert(name);
*/
</script>

<script>
function conf(){
    var con = confirm("Are you sure want to delete?");
    if(con){
        alert ("you clicked ok");
    } else
        alert ("you clicked cancel");
    }
</script>

<a href="" onclick="conf()">Delete</a>
