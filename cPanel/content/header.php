<?php require "session.php"; ?>
<?php
    require "class/database.php";
    $obj = new Database();
?>
<!DOCTYPE html>
<html>
<head>

<title>ePeepal ::: On-line Shopping</title>
<link rel="icon" type="image/png" id="favicon" href="../images/ep_logo.png" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script src="js/jquery.js"></script>
<script type="text/javascript">
    function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
</script>
</head>

<body>

<div class="header_wrap">
<?php if(isset($_SESSION['sAdmin'])){
            echo $_SESSION['sAdmin'];

        }
        else{
            echo $_SESSION['admin'];
        }



 ?>

    <div class="top_border">
        <p><?php date_default_timezone_set("Asia/Kathmandu"); echo date("D, d M Y, h:i:s A"); ?></p>
    	<ul>       	
            <li><a href="logout">Logout</a></li>
            <li>Welcome : <strong><?php echo $_SESSION['user_name'];?></strong></li>
        </ul>
    </div>
    
    <div class="header">
    	<a href="../../index" title="Visit Store" target="_blank">
            <div class="logo"></div>
        </a>
        
        <form method="POST" action="index.php?page=search">
        <div class="search_wrap">
                <select>
                    <option>--Choose--</option>
                    <option>User</option>
                    <option>Product</option>
                    <option>Category</option>
                    <option>Customer</option>
                </select>
                <input type="text" name="search" placeholder="Search" class="search" />
        </div>
        </form>
        
    </div>
    
</div>