<?php 
    $id = $_GET['id'];

    $result = $obj->selectRequiredRow_Sub("group_table","cat_id",$id,"name");
    $result2 = $obj->selectRequiredRow("category","category_id",$id);
?>
<div class="page_tittle">Category >> <?php echo $result2["name"]; ?></div>
    
    <div class="clear"></div>
    <p class="flag"><?php if(isset($_GET['flag'])) { ?> <?php echo $_GET['flag'] ?> <?php } ?></p>
    
    <form method="POST" action="process">

    <div class="loop_table">

        <div class="top_action">           
            <ul>              
                <li><a href="?page=add_group&id=<?php echo $_GET['id']?>">Add New Group</a></li>  
            </ul>
        </div>

        <div class="clear"></div>

        <table>            
            <tr>
                <th>SN</th>
                <th>Group Name</th>
                <th>Status</th>
                <th>Sub Category</th>
                <th style="width:100px"></th>
            </tr>
            
            <?php $i=1; foreach ($result as $row) { ?>
           	<tr>
                <td><?php echo $i++ ?>	</td>
                <td><?php echo $row['name']; ?></td>
                <td><?php if($row['status']==1){echo "active"; } else {echo "inactive"; } ?></td>
                <td><a href="?page=sub_category&cid=<?php echo $id ?>&gid=<?php echo $row['id']; ?>">View</td>
                <td>
                	<a href="?page=edit_group&cat_id=<?php echo $id?>&id=<?php echo $row['id']; ?>">Edit</a> |
                    <a href="javascript:void()" onClick='deleteGroup("<?php echo  $row['id']; ?>")'>Delete</a>
                </td>
            </tr>
            <?php } ?>
		</table>
	</div>
	</form>
<script>
 function deleteGroup(argu){
	var ok = confirm("Are you sure want to delete?"); 
	if(ok){
		window.location = "category/process?sub_operation=group_delete&ct=sub_category&row=group_id&cat_id=<?php echo $id?>&id="+argu;	
	}
 }
</script>
