<?php 
    $cid = $_GET['cid'];
    $gid = $_GET['gid'];
    $sid = $_GET['sid'];

    $c = $obj -> selectRequiredRow("category","category_id",$cid);
    $g = $obj -> selectRequiredRow("group_table","id",$gid);
    $s = $obj -> selectRequiredRow("sub_category","id",$sid);
?>

<p class="page_tittle">Category >> Edit Sub Category >> <?php echo $c['name'] ?> >> <?php echo $g['name'] ?></p>

<div class="add_wrap">
    <form method="POST" action="category/process">
    <input type="hidden" name="sub_operation" value="sub_edit" />
    <input type="hidden" name="sid"  value="<?php echo $sid; ?>" />
    <input type="hidden" name="gid" value="<?php echo $gid; ?>" />
    <input type="hidden" name="cid" value="<?php echo $cid; ?>" />
        <table>
           
            <tr>
                <td width="160">Sub Category Name</td>
                <td width="10"> : </td>
                <td><input type="text" name="sub_cat_name" value="<?php echo $s['name'];?>" required="require" /></td>    
            </tr>

            
            <tr>
                <td>Inactive</td>
                <td> : </td>
                <td><input type="checkbox" name="sub_cat_status" <?php if($s['status']==0){echo 'checked="checked"';} else {echo "";} ?> /></td>    
            </tr>

        </table>
    
        <div><input type="submit" name="submit" value="Submit" class="add_btn" /></div> 
        
    </form>    
</div>