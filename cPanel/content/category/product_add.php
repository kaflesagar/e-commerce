<?php 
    $cid = $_GET['cid'];
    $gid = $_GET['gid'];
    $sid = $_GET['sid'];

    $c = $obj -> selectRequiredRow("Category","category_id",$cid);
    $g = $obj -> selectRequiredRow("group_table","id",$gid);
    $s = $obj -> selectRequiredRow("sub_category","id",$sid);

?>
<div class="page_tittle">Category >> Add Product >> <strong> <?php echo $c["name"]; ?> >> <?php echo $g["name"]; ?> >> <?php echo $s["name"]; ?> </strong></div>

<div class="add_wrap">
    <form method="POST" action="category/process" enctype="multipart/form-data">
    <input type="hidden" name="operation" value="add_product" />
    <input type="hidden" name="cid" value="<?php echo $cid ?>" />
    <input type="hidden" name="gid" value="<?php echo $gid ?>" />
    <input type="hidden" name="sid" value="<?php echo $sid ?>"/>
    <input type="hidden" name="category" value="<?php echo $c['name'] ?>" />
    <input type="hidden" name="group_name" value="<?php echo $g['name'] ?>" />
    <input type="hidden" name="sub_category" value="<?php echo $s['name'] ?>" />
    <input type="hidden" name="product_code" />
    <input type="hidden" name="entry_date" value="<?php date_default_timezone_set("Asia/Kathmandu"); echo date("d m Y h:i:s"); ?>" />
        <table>    

            <tr>
                <td width="160">Product Name</td>
                <td width="10"> : </td>
                <td><input type="text" name="product_name" required /></td>    
            </tr>

            <tr>
                <td>Image 1 (Main Image)</td>
                <td> : </td>
                <td><input type="file" name="product_img_1" required/></td>
            </tr>

            <tr>
                <td>Image 2</td>
                <td> : </td>
                <td><input type="file" name="product_img_2" required/></td>
            </tr>

            <tr>
                <td>Image 3</td>
                <td> : </td>
                <td><input type="file" name="product_img_3" required/></td>
            </tr>

            <tr>
                <td>Image 4</td>
                <td> : </td>
                <td><input type="file" name="product_img_4" required/></td>
            </tr>

            <tr>
                <td>Price (NPR)</td>
                <td> : </td>
                <td><input type="text" name="product_price" onkeypress='return isNumberKey(event)' required/></td>    
            </tr>

            <tr>
                <td>Discount (%)</td>
                <td> : </td>
                <td><input type="text" name="discount" onkeypress='return isNumberKey(event)' required/></td>    
            </tr>

            <tr>
                <td>Color</td>
                <td> : </td>
                <td><input type="text" name="color" required/></td>    
            </tr>
                          
            <tr class="cbox">
                <td>Size</td>
                <td> : </td>
                <td>
                    <label><input type="checkbox" name="size1"><span>EL</span></label>
                    <label><input type="checkbox" name="size2"><span>L</span></label>
                    <label><input type="checkbox" name="size3"><span>M</span></label>
                    <label><input type="checkbox" name="size4"><span>S</span></label>
                    <label><input type="checkbox" name=""><span>N/A</span></label>
                </td>    
            </tr>

            <tr>
                <td></td>
                <td></td>
                <td><i>EL = Extra Large, L = Large, M = Medium, S = Small, N/A = Not Applicable</i></td>
            </tr>

            <tr>
                <td>Stock (Nos)</td>
                <td> : </td>
                <td><input type="text" name="stock" onkeypress='return isNumberKey(event)' required/></td>
            </tr>

            <tr>
                <td>Product Caption</td>
                <td> : </td>
                <td><textarea name="product_caption" cols="60" rows="6" required></textarea></td>    
            </tr>

            <tr>
                <td>Material & Care</td>
                <td> : </td>
                <td><textarea name="material_care" cols="60" rows="5" required></textarea></td>    
            </tr>

            <tr>
                <td>Product Description</td>
                <td> : </td>
                <td><textarea name="product_desc" cols="60" rows="10" required></textarea></td>    
            </tr>

            <tr>
                <td>Other Details</td>
                <td> : </td>
                <td><textarea name="other_details" cols="60" rows="5" required></textarea></td>    
            </tr>
            
        </table>
    
        <div><input type="submit" name="submit" value="Add Product" class="add_btn" /></div> 
        
    </form>    
</div>