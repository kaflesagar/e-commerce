<?php
    $id = $_GET['id'];
    $result2 = $obj->selectRequiredRow("category","category_id",$id);
?>
    
    <div class="page_tittle">Category >> Add Group >> <?php echo $result2["name"]; ?></div>
    
	<p class="flag"><?php if(isset($_GET['flag'])) { ?> <?php echo $_GET['flag'] ?> <?php } ?></p>
    
    <div class="add_wrap">
        <form method="POST" action="category/process?id=<?php echo $_GET['id']; ?>">
        <input type="hidden" name="sub_operation" value="group_add" />
  
            <table>
                <tr>
                    <td width="160">Group Name</td>
                    <td width="10"> : </td>
                    <td><input type="text" name="group_name" required="required" /></td>    
                </tr>
                            
                <tr>
                    <td>Inactive</td>
                    <td> : </td>
                    <td><input type="checkbox" name="group_status" /></td>    
                </tr>
            </table>
        
            <div><input type="submit" name="submit" value="Add Category" class="add_btn" /></div> 
            
        </form>    
    </div>