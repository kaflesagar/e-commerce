<?php 
$cid = $_GET['cid'];
$gid = $_GET['gid'];

$c = $obj->selectRequiredRow("category","category_id",$cid);
$g = $obj->selectRequiredRow("group_table","id",$gid);
$s = $obj->selectRequiredRow_sub("sub_category","group_id",$gid,"name");


?>
    <p class="page_tittle">Category >> <?php echo $c['name'] ?> >> <?php echo $g['name']; ?> </p>

    <div class="clear"></div>
    <p class="flag"><?php if(isset($_GET['flag'])) { ?> <?php echo $_GET['flag'] ?> <?php } ?></p>
    
    <form method="POST" action="process">

    <div class="loop_table">
    
        <div class="top_action">
            <ul>              
                <li><a href="?page=add_sub_category&gp_id=<?php echo $gid; ?>&cat_id=<?php echo $_GET['cid']; ?>">Add Sub Catagory</a></li>
            </ul>
        </div>

        <div class="clear"></div>

        <table>
            
            <tr>
                <th>SN</th>
                <th>Sub Category</th>
                <th>Status</th>
                <th> </th>
                <th> </th>
                <th style="width:100px"></th>
            </tr>
            
            <?php $i=1; foreach ($s as $row) { ?>
           	<tr>
                <td><?php echo $i++ ?>	</td>
                <td><?php echo $row['name']; ?></td>
                <td><?php if($row['status']==1){echo "active"; } else {echo "inactive"; } ?></td>
                <td><a href="?page=view_product&cid=<?php echo $cid?>&gid=<?php echo $gid; ?>&sid=<?php echo $row['id']; ?>">View All Product</a></td>
                <td><a href="?page=product_add&cid=<?php echo $cid?>&gid=<?php echo $gid; ?>&sid=<?php echo $row['id']; ?>">Add Product</a></td>
                <td>
                	<a href="?page=edit_sub_category&cid=<?php echo $cid?>&gid=<?php echo$gid; ?>&sid=<?php echo $row['id']; ?>">Edit</a> |
                    <a href="javascript:void(0);" onclick='deleteSubCat("<?php echo $row['id']; ?>")'>Delete</a>
                </td>
            </tr>
            <?php } ?>
		</table>  
<script>
 function deleteSubCat(argu){
    var ok = confirm("Are you sure want to delete?"); 
    if(ok){
        window.location = "category/process?sub_operation=sub_delete&ct=product&row=sub_category_id&gid=<?php echo $gid ?>&cid=<?php echo $cid ?>&sid="+argu;   
    }
 }
</script>

