<?php 
   
$start  = 0;
$end    = 20;

if(isset($_GET['pagenum'])){
    $id = $_GET['pagenum'];
    $start = ($id-1)*$end;
}

$pagelimit   = $obj -> pagination("category","name",$start,$end);
$totalcount  = $obj -> pageCount("category",$end);
?>

<div class="page_tittle"><?php if (isset($_GET['page'])); echo $_GET['page']; ?></div>
    
    <div class="clear"></div>
    <p class="flag"><?php if(isset($_GET['flag'])) { ?> <?php echo $_GET['flag'] ?> <?php } ?></p>
    
    <form method="POST" action="process">
    <div class="loop_table">
         <!--
        <div class="top_action">
           
            <ul>              
                <li><a href="?page=add_category">Add New Catagory</a></li>                         
                <li><a href="">Import</a></li>
                <li><a href="">Export</a></li>                
                <li><a href="">Delete</a></li>     
            </ul>
        </div>
        -->
        <div class="clear"></div>

        <table>
            
            <tr>
                <th>SN</th>
                <th>Cat. ID</th>
                <th>Category Name</th>
                <th>Description</th>
                <th>Group & Sub-Category</th>
                <th>Status</th>
                <th style="width:100px"></th>
            </tr>
            
            <?php $i=0; foreach ($pagelimit as $list) { $i++ ?>
            
            <tr>
                <td><?php echo $i; ?></td>
                <td><?php echo $list['category_id'] ?></td>
                <td><?php echo $list['name'] ?></td>
                <td><?php echo $list['description'] ?></td>
                <td><a href="?page=group&id=<?php echo $list['category_id']; ?>">View</a></td>
                <td><?php if($list['status']==1){echo "Active";} else {echo "Inactive";} ?></td>
                <td>
                    <a href="?page=edit_category&id=<?php echo $list['category_id']; ?>">Edit</a>
                    | 
                    <a href="javascript:void(0);" onclick='deletecategory("<?php echo $list['category_id']; ?>")'>Delete</a>
                    
                </td>
            </tr>
            
            <?php } ?>
            
        </table>
        <p class="page_number">
            <?php for($i = 1; $i <= $totalcount; $i++){ ?>
                <a href="?page=category&pagenum=<?php echo $i; ?>">
                    <?php echo $i; ?>
                </a>
            <?php } ?>
        </p>
    </div>
    </form>
    <script type="text/javascript">
        function deletecategory(argu){
            var ok = confirm("sure?");
            if(ok) {
                window.location = "category/process?operation=delete&ct=group_table&row=cat_id&id="+argu;
            }

        }
    </script>
