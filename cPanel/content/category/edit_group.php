<?php 
    $id = $_GET['id'];
    $cat_id = $_GET['cat_id'];
    $result = $obj -> selectRequiredRow("group_table","id",$id);
?>

<p class="page_tittle">Category >> Edit Sub Category</p>

<div class="add_wrap">
    <form method="POST" action="category/process">
    <input type="hidden" name="sub_operation" value="group_edit" />
    <input type="hidden" name="id"  value="<?php echo $id; ?>" />
    <input type="hidden" name="cat_id" value="<?php echo $cat_id; ?>" />
        <table>
           
            <tr>
                <td width="160">Sub Category Name</td>
                <td width="10"> : </td>
                <td><input type="text" name="group_name" value="<?php echo $result['name'];?>" required="required" /></td>    
            </tr>
            
            <tr>
                <td>Inactive</td>
                <td> : </td>
                <td><input type="checkbox" name="group_status" <?php if($result['status']==0){echo 'checked="checked"';} else {echo "";} ?> /></td>    
            </tr>

        </table>
    
        <div><input type="submit" name="submit" value="Submit" class="add_btn" /></div> 
        
    </form>    
</div>