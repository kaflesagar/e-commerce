<?php 
	$cid = $_GET['cid'];
	$gid = $_GET['gid'];
	$sid = $_GET['sid'];

	$start  = 0;
	$end    = 20;

	if(isset($_GET['pagenum'])){
	    $id = $_GET['pagenum'];
	    $start = ($id-1)*$end;
	}

	$pagelimit   = $obj -> paginationData("product","name","sub_category_id",$sid,$start,$end);
	$totalcount  = $obj -> pageCount("product",$end);

	$c = $obj->selectRequiredRow("category","category_id",$cid);
    $g = $obj->selectRequiredRow("group_table","id",$gid);
	$s = $obj->selectRequiredRow("sub_category","id",$sid);
?>
    <div class="page_tittle">Category >> Add Product >> <strong> <?php echo $c["name"]; ?> >> <?php echo $g["name"]; ?> >> <?php echo $s["name"]; ?> </strong></div>
    
    <div class="clear"></div>
    <p class="flag"><?php if(isset($_GET['flag'])) { ?> <?php echo $_GET['flag'] ?> <?php } ?></p>

    <div class="loop_table">
    
        <div class="top_action">
            <ul>
                <li><a href="?page=product_add&cid=<?php echo $cid?>&gid=<?php echo $gid; ?>&sid=<?php echo $sid; ?>">Add New Product</a></li>
                <li><a href="">Export</a></li>      
            </ul>
        </div>
        
        <div class="clear"></div>
        
        <table>  
            <tr>
                <th>SN</th>
                <th>Product Code</th>
                <th>Product Name</th>
                <th>Image Thumb</th>
                <th>Category </th>
                <th>Sub Category </th>
                <th>Price</th>
                <th>Stock</th>
                <th>Details</th>
                <th>&nbsp;</th>
                <th>Select</th>
            </tr>
            
            <?php $i=1; foreach ($pagelimit as $list) { ?>
            
            <tr>
                <td><?php echo $i++; ?></td>
                <td><?php echo $list['product_code']; ?></td>
                <td><?php echo $list['name']; ?></td>
                <td><img src="../../product_img/<?php echo $list['photo1'] ?>" width="40px" height="40px" /></td>
                <td><?php echo $list['category']; ?></td>
                <td><?php echo $list['sub_category']; ?></td>
                <td><?php echo $list['price']; ?></td>
                <td><?php echo $list['stock']; ?></td>
                <td><a href="?page=product_details&cid=<?php echo $cid ?>&gid=<?php echo $gid; ?>&sid=<?php echo $sid ?>&pid=<?php echo $list['product_id']; ?>">View</a></td>
                <td>
                    <a href="?page=edit_product&cid=<?php echo $cid; ?>&gid=<?php echo $gid ?>&sid=<?php echo $sid; ?>&pid=<?php echo $list['product_id'] ?>">Edit</a> | 
                    <a href="javascript:void(0);" onclick='deleteproduct("<?php echo $list['product_id']; ?>");'>Delete</a>
                </td>
                <td><input type="checkbox" name="checkbox[]" value="<?php echo $list['product_id'] ?>" /></td>
            </tr>

            <?php } ?>

        </table>

        <p class="page_number">
            <?php for($i = 1; $i <= $totalcount; $i++){ ?>
            <a href="?page=product&pagenum=<?php echo $i; ?>">
                <?php echo $i; ?>
            </a>
            <?php } ?>
        </p>

    </div>

<script type="text/javascript">

function deleteproduct(argu){
    var ok = confirm("Are you sure want to delete?");
    if(ok){
        window.location = "category/process?operation=product_delete&cid=<?php echo $cid?>&gid=<?php echo $gid?>&sid=<?php echo $sid?>&id="+argu;
    }
}

</script>