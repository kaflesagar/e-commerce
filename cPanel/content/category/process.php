<?php 
require "../class/database.php";
$obj = new Database();

//------------------------------>
if(isset($_REQUEST['operation'])){
$operation_type = null;
$operation_type = $_REQUEST['operation'];

    switch($operation_type){
        
        case 'add':
            $cat_name = mysql_real_escape_string(strtoupper($_POST['cat_name']));
            $cat_desc = mysql_real_escape_string($_POST['cat_desc']);
            if($_POST['cat_status']==''){
                $cat_status = 1;
            } else {
                $cat_status = 0;
            }
            $data = array(    
                'name' => $cat_name,
                'description' => $cat_desc,
                'status' => $cat_status,
            );
            
            $obj -> insert("category", $data);
            header("location:../?page=category&flag=Category Added Sucessfully");
            break;
            
         case 'edit':
            $id = $_REQUEST['id'];
            
            if($_REQUEST['cat_status']==''){
            $cat_status = 1;
            } else {
                $cat_status =0;
            }                
            $data = array(
                "name"          => $_REQUEST['cat_name'],
                "description"   => $_REQUEST['cat_desc'],
                "status"        => $cat_status
            );

            $obj -> editRow("category", $data, "category_id", $id);
            header("location:../?page=category&flag=Category Updated Sucessfully"); 
        break;
                   
        case 'deleteold':
            $id = $_GET['id'];
            
            $obj->deleteRecord("category","category_id",$id);
            header("location:../?page=category&flag=Category deleted successfully");  
        break;
           
        case 'delete':
        
            $id     = $_GET['id'];
            $ct     = $_GET['ct'];
            $field  = $_GET['row'];

            $sql = "SELECT * FROM $ct WHERE $field = $id";

            $result = mysql_query($sql);
            if(mysql_num_rows($result) > 0) {
                header("location:../?page=category&flag=Failed, Please deleted all Group first");  
            }   else {
                $obj->deleteRecord("category","category_id",$id);
                header("location:../?page=category&flag=Category deleted successfully");  
            }
            
        break;

        case 'add_product':
            if($_REQUEST['product_code']==''){
            $product_code = mt_rand(0000000000,9999999999);
            }
            
            $filename1   = $_FILES['product_img_1']['name'];
            $random1     = rand(000000000,999999999);
            $filename1   = $random1.'-'.$filename1;
            $temp1       = $_FILES['product_img_1']['tmp_name'];
            $per1        = '../../../product_img/'.$filename1;
            move_uploaded_file($temp1,$per1);

            $filename2   = $_FILES['product_img_2']['name'];
            $random2     = rand(000000000,999999999);
            $filename2   = $random2.'-'.$filename2;
            $temp2       = $_FILES['product_img_2']['tmp_name'];
            $per2        = '../../../product_img/'.$filename2;
            move_uploaded_file($temp2,$per2);

            $filename3   = $_FILES['product_img_3']['name'];
            $random3     = rand(000000000,999999999);
            $filename3   = $random3.'-'.$filename3;
            $temp3       = $_FILES['product_img_3']['tmp_name'];
            $per3        = '../../../product_img/'.$filename3;
            move_uploaded_file($temp3,$per3);  

            $filename4   = $_FILES['product_img_4']['name'];
            $random4     = rand(000000000,999999999);
            $filename4   = $random4.'-'.$filename4;
            $temp4       = $_FILES['product_img_4']['tmp_name'];
            $per4        = '../../../product_img/'.$filename4;
            move_uploaded_file($temp4,$per4);     

            $cid = $_REQUEST['cid'];
            $gid = $_REQUEST['gid'];
            $sid = $_REQUEST['sid'];
            $category       = $_REQUEST['category'];
            $group_name     = $_REQUEST['group_name'];
            $sub_category   = $_REQUEST['sub_category'];

            $data = array (
            'product_code'      => mysql_real_escape_string($product_code),
            'category'          => mysql_real_escape_string($category),
            'category_id'       => mysql_real_escape_string($_REQUEST['cid']),
            'group_name'        => mysql_real_escape_string($group_name),
            'group_id'          => mysql_real_escape_string($_REQUEST['gid']),
            'sub_category'      => mysql_real_escape_string($sub_category),
            'sub_category_id'   => mysql_real_escape_string($_REQUEST['sid']),
            'name'              => mysql_real_escape_string($_REQUEST['product_name']),
            'photo1'            => mysql_real_escape_string($filename1),
            'photo2'            => mysql_real_escape_string($filename2),
            'photo3'            => mysql_real_escape_string($filename3),
            'photo4'            => mysql_real_escape_string($filename4),
            'price'             => mysql_real_escape_string($_REQUEST['product_price']),
            'discount'          => mysql_real_escape_string($_REQUEST['discount']),
            'color'             => mysql_real_escape_string($_REQUEST['color']),
            'size1'             => mysql_real_escape_string($_REQUEST['size1']),
            'size2'             => mysql_real_escape_string($_REQUEST['size2']),
            'size3'             => mysql_real_escape_string($_REQUEST['size3']),
            'size4'             => mysql_real_escape_string($_REQUEST['size4']),
            'stock'             => mysql_real_escape_string($_REQUEST['stock']),
            'product_caption'   => mysql_real_escape_string($_REQUEST['product_caption']),
            'material_care'     => mysql_real_escape_string($_REQUEST['material_care']),
            'product_desc'      => mysql_real_escape_string($_REQUEST['product_desc']),
            'other_details'     => mysql_real_escape_string($_REQUEST['other_details']),
            'entry_date'        => mysql_real_escape_string($_REQUEST['entry_date'])
            );
            print_r($data); die();
            $obj->insert("product",$data);
            header("location:../?page=view_product&cid=$cid&gid=$gid&sid=$sid&flag=Product added successfully");
        break;

        case 'product_delete':
            $id = $_REQUEST['id'];
            $cid = $_REQUEST['cid'];
            $gid = $_REQUEST['gid'];
            $sid = $_REQUEST['sid'];

            $img = $obj->selectRequiredRow("product","product_id",$id);
            $p1 = '../../../product_img/'.$img['photo'];
            $p2 = '../../../product_img/'.$img['photo2'];
            $p3 = '../../../product_img/'.$img['photo3'];            

            unlink($p1); unlink($p2); unlink($p3);

            $obj->deleteRecord("product","product_id",$id);       
            header("location:../?page=view_product&cid=$cid&gid=$gid&sid=$sid&flag=Product deleted successfully");
        break;

        default:
        echo "Page not found";

    }
}
	
//------------------------------>

if(isset($_REQUEST['sub_operation'])){
$sub_operation_type = null;
$sub_operation_type = $_REQUEST['sub_operation'];

    switch($sub_operation_type){
        
        case 'sub_add':
            $cid = $_REQUEST['cat_id'];
			$gid = $_REQUEST['gp_id'];

			if($_POST['sub_cat_status']==''){
                $sub_cat_status = 1;
            } else {
                $sub_cat_status = 0;
            }
			
			$data = array(				
				'cat_id'	=> $cid,
                'group_id'  => $gid,
				'name'		=> mysql_real_escape_string($_REQUEST['sub_cat_name']),
				'status'	=> $sub_cat_status			
			);
			
			$obj -> insert("sub_category", $data);
            header("location:../?page=sub_category&gid=$gid&cid=$cid&flag=Sub Category Added Sucessfully");
        break;
            
        case 'sub_edit':
			$sid      = $_REQUEST['sid'];
			$cid     = $_REQUEST['cid'];            
            $gid     = $_REQUEST['gid'];

			if($_REQUEST['sub_cat_status']==''){
                $sub_cat_status = 1;
            } else {
                $sub_cat_status = 0;
            }
			
			$data = array(				
				'name'			=> mysql_real_escape_string($_REQUEST['sub_cat_name']),
				'status'		=> $sub_cat_status			
			);
			
            
			$obj->editRow("sub_category", $data,"id", $sid);
            header("location:../?page=sub_category&cid=$cid&gid=$gid&flag=Sub Category updated successfully");
        break;

        case 'sub_delete':

            $sid     = $_GET['sid'];            
            $gid     = $_GET['gid'];
            $cid     = $_GET['cid'];
            $ct     = $_GET['ct'];
            $field  = $_GET['row'];

            $sql = "SELECT * FROM $ct WHERE $field = $sid";
            //print_r($sql); die;
            $result = mysql_query($sql);
            if(mysql_num_rows($result) > 0) {
                header("location:../?page=sub_category&gid=$gid&id=$cid&flag=Failed, Please delete all Product First");
            }   else {
                $obj->deleteRecord("sub_category","id",$sid);
                header("location:../?page=sub_category&gid=$gid&cid=$cid&flag=Sub Category deleted successfully");  
            }
            
        break;

        case 'group_add':
            $id = $_REQUEST['id'];

            if($_POST['group_status']==''){
                $status = 1;
            } else {
                $status = 0;
            }
            
            $data = array(                
                'name'     => mysql_real_escape_string($_REQUEST['group_name']),
                'cat_id'   => $id,
                'status'   => $status          
            );
            
            //print_r($data);
            $obj -> insert("group_table",$data);
            header("location:../?page=group&id=$id&flag=Sub Category Added Sucessfully");
        break;

        case 'group_edit':

            $id = $_REQUEST['id'];
            $cat_id = $_REQUEST['cat_id'];

            if($_POST['group_status']==''){
                $status = 1;
            } else {
                $status = 0;
            }
            
            $data = array(              
                'name'          => mysql_real_escape_string($_REQUEST['group_name']),
                'status'        => $status          
            );
            
            $obj->editRow("group_table", $data,"id", $id);
            header("location:../?page=group&id=$cat_id&flag=Group updated successfully");
        break;

        case 'group_delete':

            $id     = $_GET['id'];
            $cat_id = $_GET['cat_id'];
            $ct     = $_GET['ct'];
            $field  = $_GET['row'];

            $sql = "SELECT * FROM $ct WHERE $field = $id";

            $result = mysql_query($sql);
            if(mysql_num_rows($result) > 0) {
                header("location:../?page=group&id=$cat_id&flag=Failed, Please delete all Sub Category First");
            }   else {
                $obj->deleteRecord("group_table","id",$id);
                header("location:../?page=group&id=$cat_id&flag=Group deleted successfully");  
            }
            
        break;

        default:
        echo "Page not found";

    }
}
?>