<?php 
    $cid    = $_GET['cat_id'];
    $gid    = $_GET['gp_id'];

    $result = $obj->selectAll("sub_category","name");
    $result2 = $obj->selectRequiredRow("category","category_id",$cid);
?>
<div class="page_tittle">Category >> Sub Category >> <?php echo $result2['name'] ?></div>
    
	<p class="flag"><?php if(isset($_GET['flag'])) { ?> <?php echo $_GET['flag'] ?> <?php } ?></p>
    
    <div class="add_wrap">
        <form method="POST" action="category/process?gp_id=<?php echo $gid ?>&cat_id=<?php echo $cid; ?>">
        <input type="hidden" name="sub_operation" value="sub_add" />
  
            <table>
                <tr>
                    <td width="160">Sub Category Name</td>
                    <td width="10"> : </td>
                    <td><input type="text" name="sub_cat_name" required="require" /></td>    
                </tr>
                            
                <tr>
                    <td>Inactive</td>
                    <td> : </td>
                    <td><input type="checkbox" name="sub_cat_status" /></td>    
                </tr>
            </table>
        
            <div><input type="submit" name="submit" value="Add Category" class="add_btn" /></div> 
            
        </form>    
    </div>