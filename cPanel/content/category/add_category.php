
<p class="page_tittle">Category >> Add New Category</p>

<div class="add_wrap">
    <form method="POST" action="category/process">
    <input type="hidden" name="operation" value="add" />
    
        <table>
            <tr>
                <td width="160">Category Name</td>
                <td width="10"> : </td>
                <td><input type="text" name="cat_name" required="" /></td>    
            </tr>
        
            <tr>
                <td>Category Description</td>
                <td> : </td>
                <td><textarea class="desc" name="cat_desc" cols="60" rows="8" required=""></textarea></td>    
            </tr>
            
            <tr>
                <td>Inactive</td>
                <td> : </td>
                <td><input type="checkbox" name="cat_status" /></td>    
            </tr>
        </table>
    
        <div><input type="submit" name="submit" value="Add Category" class="add_btn" /></div> 
        
    </form>    
</div>