<?php 
$id = $_GET['id'];
$result = $obj -> selectRequiredRow("category","category_id",$id);
?>

<p class="page_tittle">Category >> Edit Category</p>

<div class="add_wrap">
    <form method="POST" action="category/process">
    <input type="hidden" name="operation" value="edit" />
    <input type="hidden" name="id"  value="<?php echo $id; ?>" />
        <table>
           
            <tr>
                <td width="160">Category Name</td>
                <td width="10"> : </td>
                <td><input type="text" name="cat_name" value="<?php echo $result["name"]; ?>" required="require" /></td>    
            </tr>
        
            <tr>
                <td>Category Description</td>
                <td> : </td>
                <td><input class="desc" name="cat_desc" value="<?php echo $result["description"]; ?>" id="edit_desc" required="require" /></td>    
            </tr>
            
            <tr>
                <td>Inactive</td>
                <td> : </td>
                <td><input type="checkbox" name="cat_status" <?php if($result['status']==0){echo 'checked="checked"';} else {echo "";} ?> /></td>    
            </tr>

        </table>
    
        <div><input type="submit" name="submit" value="Submit" class="add_btn" /></div> 
        
    </form>    
</div>