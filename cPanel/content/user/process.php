<?php

require "../class/database.php";
$obj = new Database();
$operation_type = null;
    
    if(isset($_REQUEST['operation'])){
        
        $operation_type = $_REQUEST['operation'];
    }

    switch($operation_type){
        
//case delete goes here...
        case 'delete':
            $id = $_REQUEST['id'];
            $result = $obj->deleteRecord("user","id",$id); 
            header("location:../?page=user&flag=User deleted sucessfully");
            break; 
            
//case insert goes here...
        case 'insert':
            $user_name  = $_POST['user_name'];
            $password   = $_POST['password'];
            $first_name = $_POST['first_name'];
            $last_name  = $_POST['last_name'];
            $email      = $_POST['email'];
            $user_type  = $_POST['user_type'];
            
            if($_POST['status']==''){
                $status = 1;
            } else {
                $status = 0;
            }
            $data = array(
                'user'        => $user_name,
                'password'    => $password,
                'first_name'  => $first_name,
                'last_name'   => $last_name,
                'email'       => $email,
                'user_type'   => $user_type,
                'status'      => $status    
            );  
            
            $obj -> insert("user", $data); 
            header('location:../?page=user&flag=User Added Sucessfully');            
            break;
        
//case Edit goes here...   
         case 'edit':
            $id = $_POST['id'];
            if($_POST['status']==''){
                $status = 1;
            } else {
                $status = 0;
            }
        
            $data = array(
            "user"          => $_POST['user_name'],
            "password"      => $_POST['password'],
            "first_name"    => $_POST['first_name'],
            "last_name"     => $_POST['last_name'],
            "email"         => $_POST['email'],
            "user_type"     => $_POST['user_type'],
            "status"        => $status    
            );
            
            $obj->editRow("user", $data, "id", $id);
            header("location:../?page=user&flag=User updated successfully");    
            break;
        
//case default goes here...  
        default:
        echo "Page not found";
    }    
    

?>