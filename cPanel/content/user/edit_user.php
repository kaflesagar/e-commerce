<?php
require "class/database.php";
$obj = new Database();

//fetch data of required table
$id = $_GET['id'];
$result = $obj -> selectRequiredRow("user","id",$id);

?>
<p class="page_tittle">User >> Edit User</p>

<div class="add_wrap">
<form method="POST" action="user/process">
<input type="hidden" name="operation" value="edit" />
<input type="hidden" name="id" value="<?php echo $id; ?>" />

    <table class="form_table">
       
        <tr>
            <td width="160">First Name</td>
            <td width="10"> : </td>
            <td><input type="text" name="first_name" value="<?php echo $result['first_name'] ?>" /></td>
        </tr>
         <tr>
            <td>Last Name</td>
            <td> : </td>
            <td><input type="text" name="last_name" value="<?php echo $result['last_name'] ?>" /></td>
        </tr>
         <tr>
            <td>Email</td>
            <td> : </td>
            <td><input type="text" name="email" value="<?php echo $result['email'] ?>" /></td>
        </tr>
        <tr>
            <td>User Name</td>
            <td> : </td>
            <td><input type="text" name="user_name" value="<?php echo $result['user'] ?>" /></td>
        </tr>
         <tr>
            <td>Password</td>
            <td> : </td>
            <td><input type="password" name="password"  required="require" /></td>
        </tr>
        <tr>
            <td>Role</td>
            <td> : </td>
            <td>
                <select name="user_type" required>
                    <option><?php if($result["user_type"]==1){echo "Admin";} else {echo "User";} ?></option>
                    <option value="1">Admin</option>
                    <option value="0">User</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Inactive</td>
            <td> : </td>
            <td><input type="checkbox" name="status" <?php if($result['status']==0){echo 'checked="checked"';} else {echo "";}?> /></td>
        </tr>
        
    </table>
    <input type="submit" name="submit" value="Update" class="add_btn" />
</form>
</div>