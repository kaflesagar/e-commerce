
<p class="page_tittle">User >> Add New User</p>

<div class="add_wrap">
<form method="POST" action="user/process">
<input type="hidden" name="operation" value="insert" />

    <table class="form_table">
        <tr>
            <td width="160">First Name</td>
            <td width="10"> : </td>
            <td><input type="text" name="first_name"  /></td>
        </tr>
         <tr>
            <td>Last Name</td>
            <td> : </td>
            <td><input type="text" name="last_name"  /></td>
        </tr>
         <tr>
            <td>Email</td>
            <td> : </td>
            <td><input type="text" name="email" /></td>
        </tr>
        <tr>
            <td>User Name</td>
            <td> : </td>
            <td><input type="text" name="user_name" /></td>
        </tr>
         <tr>
            <td>Password</td>
            <td> : </td>
            <td><input type="password" name="password" /></td>
        </tr>
        <tr>
            <td>Role</td>
            <td> : </td>
            <td>
                <select name="user_type" >
                    <option></option>
                    <option value="1">Admin</option>
                    <option value="2">User</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Inactive</td>
            <td> : </td>
            <td><input type="checkbox" name="status" /></td>
        </tr>
       
    </table>
    <input type="submit" name="submit" value="Add User" class="add_btn" />
</form>
</div>