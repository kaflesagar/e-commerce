<?php 

$result = $obj -> selectAll("user","user");

if(isset($_POST['checkbox'])){
    
    $checkbox=$_POST['checkbox'];
    $id = implode(',',$checkbox);
    
    $obj->deleteSelectedRecord("user","id",$id); 
    header("location:index.php?page=user&flag=Category deleted successfully");  
}

?>

<div class="page_tittle"><?php if (isset($_GET['page'])); echo $_GET['page']; ?></div>

    <div class="clear"></div>
    <p class="flag"><?php if(isset($_GET['flag'])) { ?> <?php echo $_GET['flag'] ?> <?php } ?></p>
    <form method="POST" action="">
    <div class="loop_table">
        <div class="top_action">
            <ul>            
                <li><a href="?page=add_user">Add New User</a></li>             
                <li><a href="?page=import_user">Import</a></li>
                <li><a href="">Export</a></li>                
                <li><a href="">Delete</a></li>     
            </ul>
        </div>
        
        <div class="clear"></div>
        
        <table>  
              
            <tr>
                <th>SN</th>
                <th>Full Name</th>
                <th>User Name</th>
                <th>Password</th>
                <th>Email</th>
                <th>Role</th>
                <th>Status</th>
                <th style="width:100px"></th>
                <th>Select</th>
            </tr>

            <?php $i=1; foreach ($result as $list){ ?>
            
            <tr>
                <td><?php echo $i++;?></td>
                <td><?php echo $list['first_name']; echo ' '; echo $list['last_name']; ?></td>
                <td><?php echo $list['user'] ?></td>
                <td><input type="text" placeholder="xxxxxxxxxxxx" class="password" disabled="disabled" /></td>
                <td><?php echo $list['email'] ?></td>
                <td><?php if($list['user_type']==1) {echo "Admin";} else {echo "User";}?></td>
                <td><?php if($list['status']==1) {echo "Active";} else {echo "Inactive";}?></td>
                <td>
                    <a href="?page=edit_user&id=<?php echo $list['id']; ?>">Edit</a> | 
                    <a href="javascript:void(0);" onclick='deleteuser("<?php echo $list['id']; ?>");'>Delete</a>
                </td>
                <td><input type="checkbox" name="checkbox[]" value="<?php echo $list['id']; ?>" /></td>
            </tr>
            
            <?php } ?>
            
        </table>
        
        
    </div>
    </form>
    
    <br />
    

    <script type="text/javascript">
    
    function deleteuser(argu){
        var ok = confirm("Are you sure want to delete?");
        if(ok){
            window.location = "user/process?operation=delete&id="+argu;
        }
    }
    
    </script>

<!---
    <div id="test">s</div>

    <script>

     $("#test").html("This is just a test");

    </script>

 --->


