<?php include "include/header.php"; ?>
<?php include "include/menu.php"; ?>
<div id="bread_crumb">Cart / Checkout / Address</div>

<div id="cart_address">
	<ul>
		<li>Country</li>
		<li>
			<select>
				<option>Select...</option>
				<option>Nepal</option>
			</select>
		</li>
	</ul>
	<ul>
		<li>Zone</li>
		<li><input type="text" name=""></li>
	</ul>	
	<ul>
		<li>District</li>
		<li><input type="text" name=""></li>
	</ul>
	<ul>
		<li>Address</li>
		<li><textarea></textarea></li>
	</ul>
	<ul>
		<li>Choose Payment Option : </li>
		<li>
			<div class="payment_option">
				<ul>
					<li><input type="image" name="" src="library/images/ewallet.jpg" alt="eWallet" value="eWallet"></li>
					<li>
						<div>
					        <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" name="_xclick">
					              <input type="hidden" name="cmd" value="_cart" />
					              <input type="hidden" name="upload" value="1" />
					              <input type="hidden" name="business" value="seller@epeepal.com"/>
					              <input type="hidden" name="currency_code" value="NPR" />

					              <?php 
					              	$apname 	= $_SESSION['apname'];
					              	$aamount 	= $_SESSION['aamount'];
					              	$aqty		= $_SESSION['aqty'];
					              ?>

					              <?php
					              foreach ($apname as $value) {
					                echo $value;
					              }
					              foreach ($aamount as $value1) {
					                echo $value1;
					              }
					              foreach ($aqty as $value2) {
					                echo $value2;
					              }
					               ?>

					              <input type="hidden" name="return" value="http://localhost/epeepal/epeepal/return_success" />
					              <input type="hidden" name="cancel_return" value="http://localhost/epeepal_old/return_cancel" />
					              <input type="image" src="library/images/paypal.jpg" alt="Make payments with PayPal - it's fast, free and secure!" name="submit" value="Paypal" />
					        </form>
					    </div>
					</li>
					<li><input type="image" src="library/images/esewa.jpg" value="eSewa"></li>
				</ul>
			</div>
		</li>
	</ul>
</div>

<div id="checkoutdetails">

		<?php foreach($_SESSION['cart'] as $row) { $c++ ?>	

		<?php 
			error_reporting(0);

			$qty = $row['qty'];
			$price = $row['price'];
			$bdamount = $qty * $price;
			$discountrate = $row['discount']/100;
			$admount =  $bdamount * $discountrate;
			$total = $bdamount - $admount;
			$carttotal += $total;
			$vat = 13;
			$vatamt = $carttotal * ($vat/100);
			$del_charge = 0;
		?>
		<?php } ?>
		<ul class="totalorder">
			<li>Total Order Items <a href="">View</a></li>
			<li><?php $c = count($_SESSION['cart']); echo $c; ?></li>
		</ul>

		<div class="clear"></div>

		<h2>Price Details :</h2>

		<ul>
			<li>Cart Total</li>
			<li><?php echo $carttotal ?></li>
		</ul>
		<ul>
			<li>Vat (<?php echo $vat; ?>%)</li>
			<li> + <?php echo $vatamt; ?></li>
		</ul>
		<ul>
			<li>Delivery Charge</li>
			<li><?php if($del_charge == "0"){ echo "Free";}else{echo "+ ".$del_charge; }?></li>
		</ul>
		<ul class="amt">
			<li>Grand Total :</li>
			<li><?php echo $carttotal + $vatamt + $del_charge ?></li>
		</ul>

	</div>
<?php include "include/footer.php"; ?>
